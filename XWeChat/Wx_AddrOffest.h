/*
	
	2.7.1.88
	所有偏移基于WeChatWin,dll模块的基址

*/

#define ADDR_WXISLOGIN_OFFEST 0x1397764;   //微信账户是否已登录

#define ADDR_GETFRIENDLISTHEADPOINTER_CALL_OFFEST 0x2CAC80;   //获取 获取好友列表二叉树头

#define ADDR_GETMESSAGE_OFFEST 0x3206A8;                     //接收消息的地址偏移
#define ADDR_GETMESSAGE_CALL_OFFEST 0x18D770;                //接收消息Call的偏移

#define ADDR_OPENPREVENTREVOKE_ADDR1_OFFEST 0x26786D;       //防撤回HOOK点
#define ADDR_OPENPREVENTREVOKE_NEXT1_OFFEST 0x267876;       //防撤回跳转点1
#define ADDR_OPENPREVENTREVOKE_PARAM_OFFEST 0x7F2D00;       //参数

#define ADDR_CLOSEPREVENTREVOKE_NEXT_OFFEST 0x267872;    //关闭防撤回, 原来下一地址

#define ADDR_OPENPREVENTREVOKE_ADDR2_OFFEST 0x2678D3;        //防撤回NOP点
#define ADDR_OPENPREVENTREVOKE_NEXT2_OFFEST 0x2678D8;       //防撤回跳转点2

#define ADDR_CLOSEPREVENTREVOKE_PARAM_OFFEST 0x267776;    //关闭防撤回, 原来jmp地址

#define ADDR_OPENPREVENTREVOKE_ADDR3_OFFEST 0x267A1C;        //防撤回提示
#define ADDR_OPENPREVENTREVOKE_NEXT3_OFFEST 0x267A23;        //防撤回提示跳转点



#define ADDR_FORWARDREVOKEMSG_ADDR_OFFEST 0xB5DB8;           //转发撤回消息hook点
#define ADDR_FORWARDREVOKEMSG_NEXT_OFFEST 0xB5DBD;           //转发撤回消息跳转点
#define ADDR_FORWADRREVOKEMSG_PARAM_OFFEST 0xB5DE0;          //转发撤回消息参数


#define ADDR_GETFRIENDINFO_CALL1_OFFEST 0x4A4550;            //获取朋友信息Call 1 的偏移
#define ADDR_GETFRIENDINFO_CALL2_OFFEST 0x53400;             //获取朋友信息Call 2 的偏移
#define ADDR_GETFRIENDINFO_CALL3_OFFEST 0x27F4F0;            //获取朋友信息Call 3 的偏移

#define ADDR_SENDTEXTMESSAGE_CALL_OFFEST 0x2FA7F0;           //发送文本消息的Call偏移

#define ADDR_SENDCARDMESSAGE_CALL1_OFFEST 0x265790;          //发送card消息的Call 1 偏移（card xml封装）
#define ADDR_SENDCARDMESSAGE_CALL2_OFFEST 0x2FA7F0;          //发送card消息的Call 2 偏移（发送card）

#define ADDR_SENDIMAGEMESSAGE_CALL1_OFFEST 0x85710;           //发送图片消息的Call 1 偏移
#define ADDR_SENDIMAGEMESSAGE_CALL2_OFFEST 0x2FA1E0;          //发送图片消息的Call 2 偏移
#define ADDR_SENDIMAGEMESSAGE_CALL3_OFFEST 0x25EE90;          //发送图片消息的Call 3 偏移

#define ADDR_SENDFILEMESSAGE_CALL1_OFFEST 0x4A4510;          //发送文件消息的Call 1 偏移
#define ADDR_SENDFILEMESSAGE_CALL2_OFFEST 0x4A4550;          //发送文件消息的Call 2 偏移
#define ADDR_SENDFILEMESSAGE_CALL3_OFFEST 0x4A4550;          //发送文件消息的Call 3 偏移
#define ADDR_SENDFILEMESSAGE_CALL4_OFFEST 0x6DFA0;           //发送文件消息的Call  4 偏移
#define ADDR_SENDFILEMESSAGE_CALL5_OFFEST 0x242EC0;          //发送文件消息的Call 5 偏移
#define ADDR_SENDFILEMESSAGE_CALL6_OFFEST 0x25EE90;          //发送文件消息的Call 6 偏移
#define ADDR_SENDFILEMESSAGE_PARAM_OFFEST 0x10FD310;         //发送文件消息的参数的偏移

#define ADDR_SENDURLMESSAGE_CALL1_OFFEST 0x410CB0;           //发送url消息的 Call 1 偏移
#define ADDR_SENDURLMESSAGE_CALL2_OFFEST 0x242BC0;           //发送url消息的 Call 2 偏移
#define ADDR_SENDURLMESSAGE_CALL3_OFFEST 0x242DD0;           //发送url消息的 Call 3 偏移
#define ADDR_SENDURLMESSAGE_PARAM_OFFEST 0x13A124C;          //发送url消息的 参数 偏移

#define ADDR_SENDSMALLAPPMESSAGE_CALL1_OFFEST 0x410CB0;           // 发送小程序消息的 Call 1 偏移
#define ADDR_SENDSMALLAPPMESSAGE_CALL2_OFFEST 0x242DD0;           // 发送小程序消息的 Call 2 偏移
#define ADDR_SENDSMALLAPPMESSAGE_CALL3_OFFEST 0x18D770;           // 发送小程序消息的 Call 3 偏移
#define ADDR_SENDSMALLAPPMESSAGE_PARAM1_OFFEST 0x13971B8;           // 发送小程序消息的 参数 1 偏移

#define ADDR_SENDSMALLAPPMESSAGE_CALL4_OFFEST 0x4A4810;           // 发送小程序消息的 Call 4 偏移
#define ADDR_SENDSMALLAPPMESSAGE_CALL5_OFFEST 0x4A4990;           // 发送小程序消息的 Call 5 偏移
#define ADDR_SENDSMALLAPPMESSAGE_CALL6_OFFEST 0x4B1470;           // 发送小程序消息的 Call 6 偏移
#define ADDR_SENDSMALLAPPMESSAGE_CALL7_OFFEST 0x4AEFE0;           // 发送小程序消息的 Call 7 偏移
#define ADDR_SENDSMALLAPPMESSAGE_CALL8_OFFEST 0xEE6D9A;           // 发送小程序消息的 Call 8 偏移
#define ADDR_SENDSMALLAPPMESSAGE_CALL9_OFFEST 0x4B1500;           // 发送小程序消息的 Call 9 偏移

#define ADDR_SENDSMALLAPPMESSAGE_PARAM2_OFFEST 0x11A7EFC;           // 发送小程序消息的 参数 2 偏移
#define ADDR_SENDSMALLAPPMESSAGE_PARAM3_OFFEST 0x11001EC;           // 发送小程序消息的 参数 3 偏移
#define ADDR_SENDSMALLAPPMESSAGE_PARAM4_OFFEST 0x13973B0;           // 发送小程序消息的 参数 4 偏移
#define ADDR_SENDSMALLAPPMESSAGE_PARAM5_OFFEST 0x1100200;           // 发送小程序消息的 参数 5 偏移

#define ADDR_SENDCHATROOMURLMESSAGE_CALL1_OFFEST 0x6A7A0;           //发送群链接的 Call 1 偏移
#define ADDR_SENDCHATROOMURLMESSAGE_CALL2_OFFEST 0x53460;           //发送群链接的 Call 2 偏移
#define ADDR_SENDCHATROOMURLMESSAGE_CALL3_OFFEST 0x271150;           //发送群链接的 Call 3 偏移

#define ADDR_OWNINFO_COUNTRY_OFFEST 0x13974B8;               //所属国家的地址偏移
#define ADDR_OWNINFO_CITY_OFFEST 0x13973E0;                  //所属城市的地址偏移
#define ADDR_OWNINFO_PROVINCE_OFFEST 0x13973C8;              //所属省份的地址偏移
#define ADDR_OWNINFO_WXID_OFFEST 0x13976E0;                  //微信ID的地址偏移
#define ADDR_OWNINFO_WXNUMBER_OFFEST 0x13976C8;              //微信号的地址偏移
#define ADDR_OWNINFO_SEX_OFFEST 0x13973C4;                   //性别的地址偏移
#define ADDR_OWNINFO_PHONENUMBER_OFFEST 0x1397310;           //手机号码的地址偏移
#define ADDR_OWNINFO_LOGINDEVICE_OFFEST 0x1397718;           //手机登录设备的地址偏移
#define ADDR_OWNINFO_NICKNAME_OFFEST 0x13972DC;              //微信昵称的地址偏移
#define ADDR_OWNINFO_SIGNATURE_OFFEST 0x13973F8;             //微信个性签名的地址偏移
#define ADDR_OWNINFO_HEADIMG_OFFEST 0x13975A4;               //微信的头像偏移
#define ADDR_OWNINFO_PUBLICKEY_OFFEST 0x1397734;             //public key的偏移
#define ADDR_OWNINFO_PRIVATEKEY_OFFEST 0x139774C;             //private key的偏移
#define ADDR_OWNINFO_UNKNOWNDATA_OFFEST 0x1397488;             //未知数据的偏移
#define ADDR_OWNINFO_EXTDEVNEWPWD_OFFEST 0x139727C;             //extdevnewpwd的偏移

#define ADDR_GETWXFILEPATH_CALL_OFFEST 0x4933B0;             //获取WeChat文件保存路径的 Call 偏移

#define ADDR_UPDATECHATROOMPOST_CALL_OFFEST 0x279300;             // 更改群公告的Call 偏移

#define ADDR_UPDATECHATROOMNAME_CALL_OFFEST 0x275AD0;             // 更改群名称的Call 偏移

#define ADDR_UPDATECHATROOMNICKNAME_CALL1_OFFEST 0x4A4810;             // 更改群昵称的 Call1 偏移
#define ADDR_UPDATECHATROOMNICKNAME_CALL2_OFFEST 0x2760B0;             // 更改群昵称的 Call2 偏移

#define ADDR_GETCHATROOMINFO_CALL1_OFFEST 0x42F060;             // 获取群信息的 Call1 偏移
#define ADDR_GETCHATROOMINFO_CALL2_OFFEST 0x2CAD80;             // 获取群信息的 Call2 偏移
#define ADDR_GETCHATROOMINFO_CALL3_OFFEST 0x435A00;             // 获取群信息的 Call3 偏移

#define ADDR_INVITEFRIENDJOINCHATROOM_CALL1_OFFEST 0x6A7A0;             // 群拉人的 Call1 偏移
#define ADDR_INVITEFRIENDJOINCHATROOM_CALL2_OFFEST 0x2712F0;            // 群拉人的 Call2 偏移
#define ADDR_INVITEFRIENDJOINCHATROOM_PARAM_OFFEST 0x1397790;            // 群拉人的 参数 偏移


#define ADDR_KICKFRIENDINCHATROOM_CALL1_OFFEST 0x4A4550;            // 群踢人的 Call1 偏移
#define ADDR_KICKFRIENDINCHATROOM_CALL2_OFFEST 0x53460;            // 群踢人的 Call2 偏移
#define ADDR_KICKFRIENDINCHATROOM_CALL3_OFFEST 0x271510;            // 群踢人的 Call3 偏移

#define ADDR_CREATECHATROOM_CALL_OFFEST 0x270F30;            // 创建群的 Call 偏移
#define ADDR_CREATECHATROOM_PARAM_OFFEST 0x1397790;            //创建群的 参数 偏移

#define ADDR_QUITCHATROOM_CALL1_OFFEST 0xB34CA4;           // 退群的 Call1 偏移
#define ADDR_QUITCHATROOM_CALL2_OFFEST 0x34B5C0;           // 退群的 Call2 偏移
#define ADDR_QUITCHATROOM_CALL3_OFFEST 0x3487A0;           // 退群的 Call3 偏移
#define ADDR_QUITCHATROOM_CALL4_OFFEST 0x349A20;           // 退群的 Call4 偏移

#define ADDR_ATTENPUBLICUSER_CALL1_OFFEST 0x4A4810;           // 关注公众号的 Call1 偏移
#define ADDR_ATTENPUBLICUSER_CALL2_OFFEST 0x27D840;           // 关注公众号的 Call2 偏移
#define ADDR_ATTENPUBLICUSER_PARAM_OFFEST 0x1397A54;           // 关注公众号的 参数 偏移

#define ADDR_ATTENPUBLICUSER_CALL3_OFFEST 0x6C170;           // 关注公众号的 Call1 偏移
#define ADDR_ATTENPUBLICUSER_CALL4_OFFEST 0x4A4810;           // 关注公众号的 Call2 偏移
#define ADDR_ATTENPUBLICUSER_PARAM1_OFFEST 0x1100528;           // 关注公众号的 参数 偏移
#define ADDR_ATTENPUBLICUSER_PARAM2_OFFEST 0x10FD310;           // 关注公众号的 参数 偏移

#define ADDR_GETMONEY_CALL1_OFFEST 0x7575D0;           // 收款的 Call1 偏移
#define ADDR_GETMONEY_CALL2_OFFEST 0x757650;           // 收款的 Call2 偏移

#define ADDR_AGREEFRIEND_CALL1_OFFEST 0x18EBC0;           // 收款的 Call1 偏移
#define ADDR_AGREEFRIEND_CALL2_OFFEST 0x527A0;           // 收款的 Call2 偏移
#define ADDR_AGREEFRIEND_CALL3_OFFEST 0xD37C0;           // 收款的 Call1 偏移
#define ADDR_AGREEFRIEND_CALL4_OFFEST 0x173ED0;           // 收款的 Call2 偏移
#define ADDR_AGREEFRIEND_PARAM_OFFEST 0x1397A24;           // 收款的 Call1 偏移

#define ADDR_ADDFRIEND_CALL1_OFFEST 0x4A4510;           // 添加好友的 Call1 偏移
#define ADDR_ADDFRIEND_CALL2_OFFEST 0x4A4550;           // 添加好友的  Call2 偏移
#define ADDR_ADDFRIEND_CALL3_OFFEST 0x53400;           // 添加好友的  Call3 偏移
#define ADDR_ADDFRIEND_CALL4_OFFEST 0x27D840;           // 添加好友的 Call4 偏移
#define ADDR_ADDFRIEND_CALL5_OFFEST 0x6C170;           // 添加好友的 Call5 偏移
#define ADDR_ADDFRIEND_CALL6_OFFEST 0x5C1E0;           // 添加好友的 Call6 偏移
#define ADDR_ADDFRIEND_PARAM_OFFEST 0x1100528;           // 添加好友的 参数 偏移

#define ADDR_DELETEDFRIEND_CALL1_OFFEST 0x5BEF0;           // 删除好友的  Call1 偏移
#define ADDR_DELETEDFRIEND_CALL2_OFFEST 0x6CDF50;           // 删除好友的 Call2 偏移

#define ADDR_DELETEDFRIEND_CALL3_OFFEST 0x4A4550;           // 删除好友的  Call1 偏移
#define ADDR_DELETEDFRIEND_CALL4_OFFEST 0x4B7500;           // 删除好友的 Call2 偏移
#define ADDR_DELETEDFRIEND_CALL5_OFFEST 0x5BDE0;           // 删除好友的  Call1 偏移
#define ADDR_DELETEDFRIEND_CALL6_OFFEST 0x4A3860;           // 删除好友的 Call2 偏移
#define ADDR_DELETEDFRIEND_CALL7_OFFEST 0xB9410;           // 删除好友的  Call1 偏移
#define ADDR_DELETEDFRIEND_CALL8_OFFEST 0x53400;           // 删除好友的 Call2 偏移
#define ADDR_DELETEDFRIEND_CALL9_OFFEST 0x2809D0;           // 删除好友的  Call1 偏移

#define ADDR_DELETEDFRIEND_PARAM1_OFFEST 0x10FD2E8;           // 删除好友的  Call1 偏移
#define ADDR_DELETEDFRIEND_PARAM2_OFFEST 0x1148B2C;           // 删除好友的  Call1 偏移
#define ADDR_DELETEDFRIEND_PARAM3_OFFEST 0x1148AC4;           // 删除好友的  Call1 偏移
#define ADDR_DELETEDFRIEND_PARAM4_OFFEST 0x1148A78;           // 删除好友的  Call1 偏移
#define ADDR_DELETEDFRIEND_PARAM5_OFFEST 0x1148AE8;           // 删除好友的  Call1 偏移

#define ADDR_GETDATABASEHANDLE_ADDR_OFFEST 0x4497F3;           // hook获取数据库句柄
#define ADDR_GETDATABASEHANDLE_NEXT_OFFEST 0x4497F9;           // hook获取数据库句柄
#define ADDR_GETDATABASEHANDLE_PARAM_OFFEST 0x44B1CC0;           // hook获取数据库句柄

#define ADDR_DATABASEEXEC_CALL_OFFEST 0x8646E0;           // 数据库语句执行call