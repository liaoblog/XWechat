#pragma once
#include "stdafx.h"
#include "include/rapidjson/document.h"
#include "Wx_GlobalData.h"

using namespace rapidjson;
typedef GenericDocument<UTF16<> > WDocument;
typedef GenericValue<UTF16<> > WValue;

struct Wx_Parse_Wx_GetInfoByWxid {


	WDocument doc;
	WValue val;
	TCHAR* wxid;

	bool Parse(TCHAR* content);

	bool Parse(WValue content);

};

struct Wx_Parse_Wx_DeleteFriend {
private:
	WDocument doc;
	WValue val;
public:
	TCHAR* wxid;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_AgreeFriend {

private:
	WDocument doc;
	WValue val;

	WValue wv1;
	WValue wv2;
public:
	TCHAR* v1;
	TCHAR* v2;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_GetChatRoomInfo {
private:
	WDocument doc;
	WValue val;
public:
	TCHAR* chatRoomId;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_UpdateChatRoomPost {
private:
	WDocument doc;
	WValue val;

	WValue wchatRoomId;
	WValue wtext;

public:
	TCHAR* chatRoomId;
	TCHAR* text;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_UpdateChatRoomName {
private:
	WDocument doc;
	WValue val;

	WValue wchatRoomId;
	WValue wnewName;
public:
	TCHAR* chatRoomId;
	TCHAR* newName;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_InviteFriendJoinChatRoom {
private:
	WDocument doc;
	WValue val;

	WValue wchatRoomId;
	WValue wlist;
public:
	TCHAR* chatRoomId;
	DWORD* inviteeWxIdListAddr;
	DWORD inviteeWxIdListLen;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_CreateChatRoom {
private:
	WDocument doc;
	WValue val;

	WValue wlist;
public:
	DWORD* inviteeWxIdListAddr;
	DWORD inviteeWxIdListLen;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_KickFriendInChatRoom {
private:
	WDocument doc;
	WValue val;

	WValue wchatRoomId;
	WValue wlist;
public:
	TCHAR* chatRoomId;
	DWORD* inviteeWxIdListAddr;
	DWORD inviteeWxIdListLen;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_QuitChatRoom {
private:
	WDocument doc;
	WValue val;
public:
	TCHAR* chatRoomId;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_SendTextMessage {
private:
	WDocument doc;
	WValue val;

	WValue wwxid;
	WValue wtext;
	WValue watlist;
public:
	TCHAR* wxid;
	TCHAR* texta;
	DWORD* atListAddr;
	DWORD atListLen;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_SendFriendCardMessage {
private:
	WDocument doc;
	WValue val;

	WValue wwxid;
	WValue wcardWxId;
public:
	TCHAR* wxid;
	TCHAR* cardWxId;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_SendImageMessage {
private:
	WDocument doc;
	WValue val;

	WValue wwxid;
	WValue wfilepath;
public:
	TCHAR* wxid;
	TCHAR* filepath;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_SendFileMessage {
private:
	WDocument doc;
	WValue val;

	WValue wwxid;
	WValue wfilepath;
public:
	TCHAR* wxid;
	TCHAR* filepath;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_SendUrlMessage {

private:
	WDocument doc;
	WValue val;

	WValue wwxid;
	
	WValue wtitle;
	WValue wdesc;
	WValue wurl;
	WValue wimgurl;
public:
	TCHAR* wxid;
	WxArticleInfo articleInfo;

	bool Parse(TCHAR* content);

};

struct Wx_Parse_Wx_SendChatRoomUrlMessage {
private:
	WDocument doc;
	WValue val;

	WValue wwxid;
	WValue wchatRoomId;
public:
	TCHAR* wxid;
	TCHAR* chatRoomId;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_SendSmallAppMessage {
private:
	WDocument doc;
	WValue val;

	WValue wwxid;

	WValue wappName;
	WValue wtitle;
	WValue wappId;
	WValue wurl;
	WValue wpagePath;
	WValue wuserName;
	WValue wweAppIconUrl;
public:
	TCHAR* wxid;
	WxSmallAppInfo smallAppInfo;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_GetMoney {
private:
	WDocument doc;
	WValue val;

	WValue wwxid;
	WValue wtransid;
public:
	TCHAR* wxid;
	TCHAR* transid;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_OpenPreventRevoke {
private:
	WDocument doc;
	WValue val;
public:
	TCHAR* text;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_DatabaseExec {
private:
	WDocument doc;
	WValue val;

	WValue wdbName;
	WValue wsql;
public:
	TCHAR* dbName;
	TCHAR* sql;

	bool Parse(TCHAR* content);
};

struct Wx_Parse_Wx_AddFriend {
private:
	WDocument doc;
	WValue val;

	WValue wwxid;
	WValue wtext;
	WValue wtype;
public:
	TCHAR* wxid;
	TCHAR* text;
	DWORD type;

	bool Parse(TCHAR* content);
};