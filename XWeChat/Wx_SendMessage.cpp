#include "stdafx.h"
#include "Wx_GlobalData.h"
#include <Windows.h>
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"
#include "Wx_Friend.h"
#include <string>
#include "Utils.h"
#include "Wx_OwnInfo.h"


using namespace std;

static DWORD sendMessageCall;

static DWORD sendCardMessageCall1;
static DWORD sendCardMessageCall2;

static DWORD sendImageMessageCall1;
static DWORD sendImageMessageCall2;
static DWORD sendImageMessageCall3;
static DWORD sendImageMessageCall4;
static DWORD sendImageMessageCall5;

static DWORD sendFileMessageCall1;
static DWORD sendFileMessageCall2;
static DWORD sendFileMessageCall3;
static DWORD sendFileMessageCall4;
static DWORD sendFileMessageCall5;
static DWORD sendFileMessageCall6;
static DWORD sendFileMessageParam;

static DWORD sendUrlMessageCall1;
static DWORD sendUrlMessageCall2;
static DWORD sendUrlMessageCall3;
static DWORD sendUrlMessageParam;

static DWORD sendChatRoomUrlMessageCall1;
static DWORD sendChatRoomUrlMessageCall2;
static DWORD sendChatRoomUrlMessageCall3;

static DWORD sendSmallAppMessageCall1;
static DWORD sendSmallAppMessageCall2;
static DWORD sendSmallAppMessageCall3;
static DWORD sendSmallAppMessageParam1;

static DWORD sendSmallAppMessageParam2;
static DWORD sendSmallAppMessageParam3;
static DWORD sendSmallAppMessageParam4;
static DWORD sendSmallAppMessageParam5;

static DWORD sendSmallAppMessageCall4;
static DWORD sendSmallAppMessageCall5;
static DWORD sendSmallAppMessageCall6;
static DWORD sendSmallAppMessageCall7;
static DWORD sendSmallAppMessageCall8;
static DWORD sendSmallAppMessageCall9;

struct AtText {

	TCHAR* text;
	DWORD textLen;
	DWORD textMaxLen;
	DWORD zero;
	DWORD zero2;
	DWORD atList;
	DWORD atListEndAddr;
	DWORD atListEndAddr2;

};

void Wx_SendSmallAppMessage(TCHAR* recvWxId, WxSmallAppInfo smallAppInfo) {

	PrintFunAddr((DWORD)Wx_SendSmallAppMessage);

	sendSmallAppMessageCall1 = (DWORD)hWeChatWin + ADDR_SENDSMALLAPPMESSAGE_CALL1_OFFEST;
	sendSmallAppMessageCall2 = (DWORD)hWeChatWin + ADDR_SENDSMALLAPPMESSAGE_CALL2_OFFEST;
	sendSmallAppMessageCall3 = (DWORD)hWeChatWin + ADDR_SENDSMALLAPPMESSAGE_CALL3_OFFEST;

	sendSmallAppMessageParam1 = (DWORD)hWeChatWin + ADDR_SENDSMALLAPPMESSAGE_PARAM1_OFFEST; 

	sendSmallAppMessageParam2 = (DWORD)hWeChatWin + ADDR_SENDSMALLAPPMESSAGE_PARAM2_OFFEST;
	sendSmallAppMessageParam3 = (DWORD)hWeChatWin + ADDR_SENDSMALLAPPMESSAGE_PARAM3_OFFEST;
	sendSmallAppMessageParam4 = (DWORD)hWeChatWin + ADDR_SENDSMALLAPPMESSAGE_PARAM4_OFFEST;
	sendSmallAppMessageParam5 = (DWORD)hWeChatWin + ADDR_SENDSMALLAPPMESSAGE_PARAM5_OFFEST;

	sendSmallAppMessageCall4 = (DWORD)hWeChatWin + ADDR_SENDSMALLAPPMESSAGE_CALL4_OFFEST;
	sendSmallAppMessageCall5 = (DWORD)hWeChatWin + ADDR_SENDSMALLAPPMESSAGE_CALL5_OFFEST;
	sendSmallAppMessageCall6 = (DWORD)hWeChatWin + ADDR_SENDSMALLAPPMESSAGE_CALL6_OFFEST;
	sendSmallAppMessageCall7 = (DWORD)hWeChatWin + ADDR_SENDSMALLAPPMESSAGE_CALL7_OFFEST;
	sendSmallAppMessageCall8 = (DWORD)hWeChatWin + ADDR_SENDSMALLAPPMESSAGE_CALL8_OFFEST;
	sendSmallAppMessageCall9 = (DWORD)hWeChatWin + ADDR_SENDSMALLAPPMESSAGE_CALL9_OFFEST;

	CHAR smallAppInfoBuff[0x7E0] = { 0 };
	*(DWORD*)(smallAppInfoBuff) = (DWORD)smallAppInfo.appId;
	*(DWORD*)(smallAppInfoBuff + 0x4) = wcslen(smallAppInfo.appId);
	*(DWORD*)(smallAppInfoBuff + 0x8) = wcslen(smallAppInfo.appId) * 2;

	*(DWORD*)(smallAppInfoBuff + 0x40) = (DWORD)smallAppInfo.title;
	*(DWORD*)(smallAppInfoBuff + 0x44) = wcslen(smallAppInfo.title);
	*(DWORD*)(smallAppInfoBuff + 0x48) = wcslen(smallAppInfo.title) * 2;

	*(DWORD*)(smallAppInfoBuff + 0x68) = (DWORD)L"view";
	*(DWORD*)(smallAppInfoBuff + 0x6c) = wcslen(L"view");
	*(DWORD*)(smallAppInfoBuff + 0x70) = wcslen(L"view") * 2;

	*(DWORD*)(smallAppInfoBuff + 0x7c) = 0x21;

	*(DWORD*)(smallAppInfoBuff + 0xa0) = (DWORD)smallAppInfo.url;
	*(DWORD*)(smallAppInfoBuff + 0xa4) = wcslen(smallAppInfo.url);
	*(DWORD*)(smallAppInfoBuff + 0xa8) = wcslen(smallAppInfo.url) * 2;

	*(DWORD*)(smallAppInfoBuff + 0x16c) = (DWORD)smallAppInfo.appName;
	*(DWORD*)(smallAppInfoBuff + 0x170) = wcslen(smallAppInfo.appName);
	*(DWORD*)(smallAppInfoBuff + 0x174) = wcslen(smallAppInfo.appName) * 2;

	*(DWORD*)(smallAppInfoBuff + 0x158) = (DWORD)wxOwnInfo.wxId;
	*(DWORD*)(smallAppInfoBuff + 0x15c) = wcslen(wxOwnInfo.wxId);
	*(DWORD*)(smallAppInfoBuff + 0x160) = wcslen(wxOwnInfo.wxId) * 2;

	*(DWORD*)(smallAppInfoBuff + 0x530) = (DWORD)smallAppInfo.pagePath;
	*(DWORD*)(smallAppInfoBuff + 0x534) = wcslen(smallAppInfo.pagePath);
	*(DWORD*)(smallAppInfoBuff + 0x538) = wcslen(smallAppInfo.pagePath) * 2;

	*(DWORD*)(smallAppInfoBuff + 0x544) = (DWORD)smallAppInfo.userName;
	*(DWORD*)(smallAppInfoBuff + 0x548) = wcslen(smallAppInfo.userName);
	*(DWORD*)(smallAppInfoBuff + 0x54c) = wcslen(smallAppInfo.userName) * 2;

	*(DWORD*)(smallAppInfoBuff + 0x544) = (DWORD)smallAppInfo.userName;
	*(DWORD*)(smallAppInfoBuff + 0x548) = wcslen(smallAppInfo.userName);
	*(DWORD*)(smallAppInfoBuff + 0x54c) = wcslen(smallAppInfo.userName) * 2;

	*(DWORD*)(smallAppInfoBuff + 0x558) = (DWORD)smallAppInfo.appId;
	*(DWORD*)(smallAppInfoBuff + 0x55c) = wcslen(smallAppInfo.appId);
	*(DWORD*)(smallAppInfoBuff + 0x560) = wcslen(smallAppInfo.appId) * 2;

	*(DWORD*)(smallAppInfoBuff + 0x584) = 0x2;

	*(DWORD*)(smallAppInfoBuff + 0x5d4) = (DWORD)smallAppInfo.weAppIconUrl;
	*(DWORD*)(smallAppInfoBuff + 0x5d8) = wcslen(smallAppInfo.weAppIconUrl);
	*(DWORD*)(smallAppInfoBuff + 0x5dc) = wcslen(smallAppInfo.weAppIconUrl) * 2;

	*(DWORD*)(smallAppInfoBuff + 0x630) = 0x31;
	*(DWORD*)(smallAppInfoBuff + 0x634) = 0x1;

	*(DWORD*)(smallAppInfoBuff + 0x640) = (DWORD)recvWxId;
	*(DWORD*)(smallAppInfoBuff + 0x644) = wcslen(recvWxId);
	*(DWORD*)(smallAppInfoBuff + 0x648) = wcslen(recvWxId) * 2;

	DWORD tempBuff = (DWORD)(smallAppInfoBuff + 0x600);
	DWORD packBuff = (DWORD)(smallAppInfoBuff + 0x668);
	DWORD shareIdBuff = (DWORD)(smallAppInfoBuff + 0x5a0);

	WxStr pathBuff;

	CHAR tempBuff2[0x20] = { 0 };
	CHAR tempBuff3[0x20] = { 0 };
	CHAR tempBuff4[0x20] = { 0 };

	__asm {

		push - 0x1
		push[sendSmallAppMessageParam2]
		mov ecx, shareIdBuff
		call[sendSmallAppMessageCall4]

		mov eax, smallAppInfo.appId
		push eax
		mov ecx, shareIdBuff
		call[sendSmallAppMessageCall5]

		push[sendSmallAppMessageParam3]
		mov ecx, shareIdBuff
		call[sendSmallAppMessageCall5]

		mov edx, [sendSmallAppMessageParam4]
		mov edx, [edx]
		lea ecx, tempBuff2
		call[sendSmallAppMessageCall6]

		push 0x0
		push - 0x1
		lea edx, tempBuff3
		mov ecx, eax
		call[sendSmallAppMessageCall7]
		add esp, 0x8

		lea eax, tempBuff3
		push [eax]
		mov ecx, shareIdBuff
		call[sendSmallAppMessageCall5]

		push[sendSmallAppMessageParam3]
		mov ecx, shareIdBuff
		call[sendSmallAppMessageCall5]

		push 0x0
		call[sendSmallAppMessageCall8]
		mov edx, 0
		push edx
		push eax
		lea ecx, tempBuff2
		call[sendSmallAppMessageCall9]
		add esp, 0xC

		push 0x0
		push - 0x1
		lea edx, tempBuff4
		mov ecx, eax
		call[sendSmallAppMessageCall7]
		add esp, 0x8

		lea eax, tempBuff4
		push [eax]
		mov ecx, shareIdBuff
		call[sendSmallAppMessageCall5]

		push[sendSmallAppMessageParam3]
		mov ecx, shareIdBuff
		call[sendSmallAppMessageCall5]
		push[sendSmallAppMessageParam5]
		mov ecx, shareIdBuff
		call[sendSmallAppMessageCall5]


		lea ecx, smallAppInfoBuff
		mov eax, packBuff
		push eax
		call [sendSmallAppMessageCall1]

		mov ecx, tempBuff
		call [sendSmallAppMessageCall3]

		push 0x1
		mov eax, tempBuff
		mov ecx, [sendSmallAppMessageParam1]
		push eax
		mov eax, [sendSmallAppMessageParam1]
		mov eax, [eax]
		call [eax + 0x14]

		lea eax, pathBuff
		push eax
		lea eax, pathBuff
		push eax
		push 0x0
		mov eax, tempBuff
		push eax
		call [sendSmallAppMessageCall2]
	}

}

void Wx_SendTextMessage(TCHAR* msgRecvId, TCHAR* text, DWORD* atListAddr, DWORD atListLen) {

	PrintFunAddr((DWORD)Wx_SendTextMessage);

	WxStr* atList = new WxStr[atListLen];

	sendMessageCall = (DWORD)hWeChatWin + ADDR_SENDTEXTMESSAGE_CALL_OFFEST;

	wstring joinText = L"";

	unsigned int i;
	for (i = 0; i < atListLen; i++) {

		atList[i].str = (TCHAR*)*(atListAddr + i);
		atList[i].countLen();
		//atList[i].len = wcslen((TCHAR*) * (atListAddr + i));
		//atList[i].maxLen = wcslen((TCHAR*) * (atListAddr + i)) * 2;
		
		WxUserInfo userInfo;
		userInfo = Wx_GetInfoByWxid(atList[i].str, 0, false);
		joinText += L"@";
		joinText += userInfo.nickName;
		joinText += L" ";

	}
	joinText.c_str();
	joinText += text;
	MessageBox(0,joinText.c_str(),0,0);
	AtText atText = { 0 };
	atText.text = (TCHAR*)joinText.c_str();
	atText.textLen = wcslen((TCHAR*)joinText.c_str());
	atText.textMaxLen = wcslen((TCHAR*)joinText.c_str()) * 2;
	atText.zero = 0;
	atText.zero2 = 0;


	if ((DWORD)atListAddr == 0) {
		atText.atList = 0;
		atText.atListEndAddr = 0;
		atText.atListEndAddr2 = 0;
	}
	else {
		atText.atList = (DWORD)&*atList;
		atText.atListEndAddr = (DWORD)&*atList + sizeof(WxStr) * atListLen;
		atText.atListEndAddr2 = atText.atListEndAddr;
	}

	WxStr msgRecv;
	msgRecv.str = msgRecvId;
	msgRecv.len = wcslen(msgRecvId);
	msgRecv.maxLen = wcslen(msgRecvId) * 2;

	CHAR ecxBuff[0x1DC] = { 0 };


	__asm {
		lea edx, msgRecvId
		lea ebx, atText
		lea eax, atText.atList
		push 0x1
		push eax
		push ebx
		lea ecx, ecxBuff
		call [sendMessageCall]
		add esp, 0xC

	}

	delete[] atList;
	delete[] atListAddr;
}

void Wx_SendFriendCardMessage(TCHAR* recvWxId, TCHAR* cardWxid) {

	PrintFunAddr((DWORD)Wx_SendFriendCardMessage);

	sendCardMessageCall1 = (DWORD)hWeChatWin + ADDR_SENDCARDMESSAGE_CALL1_OFFEST;
	sendCardMessageCall2 = (DWORD)hWeChatWin + ADDR_SENDCARDMESSAGE_CALL2_OFFEST;

	CHAR friendBuff[0x3E0] = { 0 };
	Wx_GetInfoByWxid(cardWxid, friendBuff);

	WxStr wxRecvWxId;
	wxRecvWxId.str = recvWxId;
	wxRecvWxId.len = wcslen(recvWxId);
	wxRecvWxId.maxLen = wcslen(recvWxId) * 2;

	WxStr wxCardxmlBuff;

	CHAR ecxBuff[0x1DC] = {0};

	__asm {

		lea edx, friendBuff
		lea ecx, wxCardxmlBuff
		call [sendCardMessageCall1]

		push 0x2A
		lea eax, wxCardxmlBuff
		lea esi, wxRecvWxId
		mov edx, esi
		push 0x0
		push eax
		lea ecx, ecxBuff
		call [sendCardMessageCall2]
		add esp, 0xC

	}
	


}

void Wx_SendImageMessage(TCHAR* recvWxId, TCHAR* filePath) {

	PrintFunAddr((DWORD)Wx_SendImageMessage);

	sendImageMessageCall1 = (DWORD)hWeChatWin + ADDR_SENDIMAGEMESSAGE_CALL1_OFFEST;
	sendImageMessageCall2 = (DWORD)hWeChatWin + ADDR_SENDIMAGEMESSAGE_CALL2_OFFEST;
	sendImageMessageCall3 = (DWORD)hWeChatWin + ADDR_SENDIMAGEMESSAGE_CALL3_OFFEST;

	WxFileStr wxMsgImage;
	wxMsgImage.msgType = 2;
	wxMsgImage.str = filePath;
	wxMsgImage.len = wcslen(filePath);
	wxMsgImage.maxLen = wxMsgImage.len * 2;

	WxStr wxRecvWxid;
	wxRecvWxid.str = recvWxId;
	wxRecvWxid.len = wcslen(recvWxId);
	wxRecvWxid.maxLen = wxRecvWxid.len * 2;

	CHAR msgBuff[0x4BC] = { 0 };

	DWORD wxMsgImageEndAddr = (DWORD)&wxMsgImage + sizeof(WxFileStr);

	__asm{
		lea ebx, wxMsgImage.str
		push ebx
		lea eax, wxRecvWxid
		push eax
		lea eax, msgBuff
		push eax
		mov ecx, wxMsgImageEndAddr
		push ecx
		call [sendImageMessageCall1]
		add esp, 0x4
		mov ecx, eax
		call [sendImageMessageCall2]
		call [sendImageMessageCall3]
	}

}

void Wx_SendFileMessage(TCHAR* recvWxId, TCHAR* filePath) {

	PrintFunAddr((DWORD)Wx_SendFileMessage);

	sendFileMessageCall1 = (DWORD)hWeChatWin + ADDR_SENDFILEMESSAGE_CALL1_OFFEST;
	sendFileMessageCall2 = (DWORD)hWeChatWin + ADDR_SENDFILEMESSAGE_CALL2_OFFEST;
	sendFileMessageCall3 = (DWORD)hWeChatWin + ADDR_SENDFILEMESSAGE_CALL3_OFFEST;
	sendFileMessageCall4 = (DWORD)hWeChatWin + ADDR_SENDFILEMESSAGE_CALL4_OFFEST;
	sendFileMessageCall5 = (DWORD)hWeChatWin + ADDR_SENDFILEMESSAGE_CALL5_OFFEST;
	sendFileMessageCall6 = (DWORD)hWeChatWin + ADDR_SENDFILEMESSAGE_CALL6_OFFEST;
	sendFileMessageParam = (DWORD)hWeChatWin + ADDR_SENDFILEMESSAGE_PARAM_OFFEST;

	WxFileStr wxMsgFile;
	wxMsgFile.msgType = 3;
	wxMsgFile.str = filePath;
	wxMsgFile.len = wcslen(filePath);
	wxMsgFile.maxLen = wxMsgFile.len * 2;

	WxStr* wxRecvWxid = new WxStr();
	wxRecvWxid->str = recvWxId;
	wxRecvWxid->len = wcslen(recvWxId);
	wxRecvWxid->maxLen = wxRecvWxid->len * 2;

	CHAR* msgBuff = new CHAR[0x4BC];

	__asm {
		mov ecx, 0
		mov byte ptr ss : [ebp - 0x5C] , cl
		push dword ptr ss : [ebp - 0x5C]
		sub esp, 0x14
		mov ecx, esp
		mov dword ptr ss : [ebp - 0x48] , esp
		push - 0x1
		push [sendFileMessageParam]
		call [sendFileMessageCall1]
		sub esp, 0x14
		mov ecx, esp
		mov dword ptr ss : [ebp - 0x60] , esp
		lea ebx, wxMsgFile.str
		push ebx
		call [sendFileMessageCall2]
		sub esp, 0x14
		mov ecx, esp
		mov dword ptr ss : [ebp - 0x64] , esp
		mov eax, wxRecvWxid
		push eax
		call [sendFileMessageCall3]
		mov eax, msgBuff
		push eax
		call [sendFileMessageCall4]
		mov ecx, eax
		call [sendFileMessageCall5]
		call [sendFileMessageCall6]

	}

	delete wxRecvWxid;
	delete[] msgBuff;

}

void Wx_SendUrlMessage(TCHAR* recvWxId, WxArticleInfo articleInfo) {

	PrintFunAddr((DWORD)Wx_SendUrlMessage);

	sendUrlMessageCall1 = (DWORD)hWeChatWin + ADDR_SENDURLMESSAGE_CALL1_OFFEST;
	sendUrlMessageCall2 = (DWORD)hWeChatWin + ADDR_SENDURLMESSAGE_CALL2_OFFEST;
	sendUrlMessageCall3 = (DWORD)hWeChatWin + ADDR_SENDURLMESSAGE_CALL3_OFFEST;
	sendUrlMessageParam = (DWORD)hWeChatWin + ADDR_SENDURLMESSAGE_PARAM_OFFEST;

	CHAR articleInfoBuff[0x5FC] = { 0 };

	TCHAR zero[] = L"0";
	*(DWORD*)(articleInfoBuff + 0x2c) = (DWORD)&zero;
	*(DWORD*)(articleInfoBuff + 0x30) = wcslen(zero);
	*(DWORD*)(articleInfoBuff + 0x34) = wcslen(zero) * 2;

	*(DWORD*)(articleInfoBuff + 0x40) = (DWORD)(articleInfo.title);
	*(DWORD*)(articleInfoBuff + 0x44) = wcslen(articleInfo.title);
	*(DWORD*)(articleInfoBuff + 0x48) = wcslen(articleInfo.title) *2;

	*(DWORD*)(articleInfoBuff + 0x54) = (DWORD)(articleInfo.desc);
	*(DWORD*)(articleInfoBuff + 0x58) = wcslen(articleInfo.desc);
	*(DWORD*)(articleInfoBuff + 0x5c) = wcslen(articleInfo.desc) * 2;

	*(DWORD*)(articleInfoBuff + 0x7c) = 5;

	*(DWORD*)(articleInfoBuff + 0xa0) = (DWORD)(articleInfo.url);
	*(DWORD*)(articleInfoBuff + 0xa4) = wcslen(articleInfo.url);
	*(DWORD*)(articleInfoBuff + 0xa8) = wcslen(articleInfo.url) * 2;

	*(DWORD*)(articleInfoBuff + 0xf0) = (DWORD)(articleInfo.imgUrl);
	*(DWORD*)(articleInfoBuff + 0xf4) = wcslen(articleInfo.imgUrl);
	*(DWORD*)(articleInfoBuff + 0xf8) = wcslen(articleInfo.imgUrl) * 2;

	*(DWORD*)(articleInfoBuff + 0x158) = (DWORD)(wxOwnInfo.wxId);
	*(DWORD*)(articleInfoBuff + 0x15c) = wcslen(wxOwnInfo.wxId);
	*(DWORD*)(articleInfoBuff + 0x160) = wcslen(wxOwnInfo.wxId) * 2;

	WxStr articleXmlBuff;

	WxStr wxRecvWxId;
	wxRecvWxId.str = recvWxId;
	wxRecvWxId.countLen();

	WxStr wxSendWxId;
	wxSendWxId.str = wxOwnInfo.wxId;
	wxSendWxId.countLen();

	CHAR buff[0x1DC] = { 0 };

	__asm {

		lea eax, articleXmlBuff
		push eax
		lea ecx, articleInfoBuff
		call [sendUrlMessageCall1]
		
		lea eax, buff
		push 5
		push eax
		lea eax, buff
		push eax
		lea eax, articleXmlBuff
		push eax
		lea eax, wxRecvWxId
		push eax
		lea eax, wxSendWxId
		push eax
		lea eax, buff
		push eax
		call [sendUrlMessageCall2]

		push [sendUrlMessageParam]
		push [sendUrlMessageParam]
		push 0x0
		lea eax, buff
		push eax
		call [sendUrlMessageCall3]


	}

}

void Wx_SendChatRoomUrlMessage(TCHAR* chatRoomId, TCHAR* recvWxId) {

	PrintFunAddr((DWORD)Wx_SendChatRoomUrlMessage);

	sendChatRoomUrlMessageCall1 = (DWORD)hWeChatWin + ADDR_SENDCHATROOMURLMESSAGE_CALL1_OFFEST;
	sendChatRoomUrlMessageCall2 = (DWORD)hWeChatWin + ADDR_SENDCHATROOMURLMESSAGE_CALL2_OFFEST;
	sendChatRoomUrlMessageCall3 = (DWORD)hWeChatWin + ADDR_SENDCHATROOMURLMESSAGE_CALL3_OFFEST;

	CHAR chatRoomInfoBuff[0x24] = {0};
	*(DWORD*)(chatRoomInfoBuff + 0x8) = (DWORD)chatRoomId;
	*(DWORD*)(chatRoomInfoBuff + 0xc) = wcslen(chatRoomId);
	*(DWORD*)(chatRoomInfoBuff + 0x10) = wcslen(chatRoomId) * 2;

	WxStr wxRecvWxId;
	wxRecvWxId.str = recvWxId;
	wxRecvWxId.len = wcslen(recvWxId);
	wxRecvWxId.maxLen = wxRecvWxId.len * 2;

	WxAddrSct wxRecvWxIdBuff;
	wxRecvWxIdBuff.startAddr = (DWORD)&wxRecvWxId;
	wxRecvWxIdBuff.endAddr = (DWORD)&wxRecvWxId + sizeof(WxStr);
	wxRecvWxIdBuff.endAddr2 = wxRecvWxIdBuff.endAddr;

	__asm {

		sub esp, 0x14
		lea ecx, chatRoomInfoBuff
		mov eax, esp
		mov dword ptr ss : [ebp - 0xBC8] , esp
		push eax
		call [sendChatRoomUrlMessageCall1]
		call [sendChatRoomUrlMessageCall2]
		lea eax, wxRecvWxIdBuff
		push eax
		call [sendChatRoomUrlMessageCall3]

	}

}