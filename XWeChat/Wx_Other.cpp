#include "stdafx.h"
#include "Wx_AddrOffest.h"
#include "Wx_GlobalData.h"
#include "Utils.h"
#include <Shlwapi.h>
#pragma comment(lib,"shlwapi.lib")

static DWORD attenPublicUserCall1;
static DWORD attenPublicUserCall2;
static DWORD attenPublicUserParam;

static DWORD attenPublicUserParam1;
static DWORD attenPublicUserParam2;
static DWORD attenPublicUserCall3;
static DWORD attenPublicUserCall4;


static DWORD getMoneyCall1;
static DWORD getMoneyCall2;

//防撤回（带提示）var 开始
static DWORD preventRevokeAddr1;
static DWORD preventRevokeNextAddr1;
static DWORD preventRevokeParam; 
static DWORD preventRevokeAddr2;
static DWORD preventRevokeNextAddr2;
static DWORD preventRevokeAddr3;
static DWORD preventRevokeNextAddr3;

static DWORD isMeRevoke = 0;

static DWORD tempTipBuff[0x6] = { 0 };
//防撤回（带提示）var  结束



//转发撤回消息var 开始

struct revokeMsg {
	DWORD revokePlace;
	DWORD revokeFrom;
	DWORD revokeContent;

};

static DWORD forwardRevokeMsgAddr;
static DWORD forwardRevokeMsgNextAddr;
static DWORD forwardRevokeMsgParam;
//转发撤回消息var 结束






//防撤回（带提示） 开始
void _preventRevoke(DWORD* revokeMsg) {
	if (StrStrW((PCWSTR)(DWORD*)*revokeMsg, L"你撤回") == NULL) {
		isMeRevoke = 0;
	}
	else {
		isMeRevoke = 1;
	}
}

void __declspec(naked) _nextAddr1() {
	__asm {
		popfd
		popad
		jmp preventRevokeNextAddr1
	}
}

void __declspec(naked) _nextAddr2() {
	__asm {
		popfd
		popad
		jmp preventRevokeNextAddr2
	}
}

void __declspec(naked) _TempStroage1() {
	__asm {

		pushad
		pushfd
		lea eax, [ebp + 8]
		push eax
		call _preventRevoke
		add esp, 4
		popfd
		popad
		call preventRevokeParam
		pushad
		pushfd
		mov eax, isMeRevoke
		test eax, eax
		jz _nextAddr1
		jmp _nextAddr2
	}
}

/*
void _preventRevokeTip(DWORD* tip) {

	//(*tip) = (DWORD)preventRevokeTip;
	memcpy((DWORD*)*tip, preventRevokeTip, wcslen(preventRevokeTip) * 2 + 2);

}
*/

void __declspec(naked) _TempStroage2() {
	__asm {
		lea eax, tempTipBuff
		push eax
		lea ecx, dword ptr ss : [ebp - 0x54]
		jmp preventRevokeNextAddr3

	}
}

bool Wx_OpenPreventRevoke(TCHAR* revokeTip) {

	PrintFunAddr((DWORD)Wx_OpenPreventRevoke);

	//memcpy(preventRevokeTip, revokeTip, wcslen(revokeTip) * 2 + 2);
	DWORD dBufSize = WideCharToMultiByte(CP_UTF8, 0, revokeTip, -1, NULL, 0, NULL, 0);
	WideCharToMultiByte(CP_UTF8, 0, revokeTip, -1, preventRevokeTip, dBufSize, NULL, FALSE);
	tempTipBuff[0] = (DWORD)preventRevokeTip;
	tempTipBuff[4] = 0x18;
	tempTipBuff[5] = 0x1F;

	if (!isPreventRevoke) {
		preventRevokeAddr1 = (DWORD)hWeChatWin + ADDR_OPENPREVENTREVOKE_ADDR1_OFFEST;
		preventRevokeNextAddr1 = (DWORD)hWeChatWin + ADDR_OPENPREVENTREVOKE_NEXT1_OFFEST;
		preventRevokeParam = (DWORD)hWeChatWin + ADDR_OPENPREVENTREVOKE_PARAM_OFFEST;
		preventRevokeAddr2 = (DWORD)hWeChatWin + ADDR_OPENPREVENTREVOKE_ADDR2_OFFEST;
		preventRevokeNextAddr2 = (DWORD)hWeChatWin + ADDR_OPENPREVENTREVOKE_NEXT2_OFFEST;
		preventRevokeAddr3 = (DWORD)hWeChatWin + ADDR_OPENPREVENTREVOKE_ADDR3_OFFEST;
		preventRevokeNextAddr3 = (DWORD)hWeChatWin + ADDR_OPENPREVENTREVOKE_NEXT3_OFFEST;

		DWORD addrHookMachineCode = (DWORD)_TempStroage1 - preventRevokeAddr1 - 5;

		BYTE machineCode[5] = { 0 };
		machineCode[0] = 0xe9;
		CHAR* addrTempHookMachineCode = (CHAR*)&addrHookMachineCode;
		machineCode[1] = *(addrTempHookMachineCode + 0);
		machineCode[2] = *(addrTempHookMachineCode + 1);
		machineCode[3] = *(addrTempHookMachineCode + 2);
		machineCode[4] = *(addrTempHookMachineCode + 3);

		BOOL flag = WriteProcessMemory((HANDLE)-1, (LPVOID) * &preventRevokeAddr1, machineCode, 5, NULL);

		//======================================================================

		BYTE machineCode2[5] = { 0x90, 0x90, 0x90, 0x90, 0x90 };

		BOOL flag2 = WriteProcessMemory((HANDLE)-1, (LPVOID) * &preventRevokeAddr2, machineCode2, 5, NULL);

		//======================================================================

		DWORD addrHookMachineCode2 = (DWORD)_TempStroage2 - preventRevokeAddr3 - 5;
		CHAR* addrTempHookMachineCode2 = (CHAR*)&addrHookMachineCode2;
		BYTE machineCode3[7] = { 0 };
		machineCode3[0] = 0xe9;
		machineCode3[1] = *(addrTempHookMachineCode2 + 0);
		machineCode3[2] = *(addrTempHookMachineCode2 + 1);
		machineCode3[3] = *(addrTempHookMachineCode2 + 2);
		machineCode3[4] = *(addrTempHookMachineCode2 + 3);
		machineCode3[5] = 0x90;
		machineCode3[6] = 0x90;

		BOOL flag3 = WriteProcessMemory((HANDLE)-1, (LPVOID) * &preventRevokeAddr3, machineCode3, 7, NULL);

		if (flag && flag2 && flag3) {
			isPreventRevoke = 1;
			return isPreventRevoke;
		}
		else {
			return isPreventRevoke;
		}
	}

	return isPreventRevoke;

}
//防撤回（带提示） 结束

static DWORD closePreventRevokeParam;
static DWORD closePreventRevokeNextAddr;
bool Wx_ClosePreventRevoke() {

	PrintFunAddr((DWORD)Wx_ClosePreventRevoke);

	if (isPreventRevoke) {

		preventRevokeAddr1 = (DWORD)hWeChatWin + ADDR_OPENPREVENTREVOKE_ADDR1_OFFEST;
		closePreventRevokeNextAddr = (DWORD)hWeChatWin + ADDR_CLOSEPREVENTREVOKE_NEXT_OFFEST;
		preventRevokeParam = (DWORD)hWeChatWin + ADDR_OPENPREVENTREVOKE_PARAM_OFFEST;

		preventRevokeAddr2 = (DWORD)hWeChatWin + ADDR_OPENPREVENTREVOKE_ADDR2_OFFEST;
		preventRevokeNextAddr2 = (DWORD)hWeChatWin + ADDR_OPENPREVENTREVOKE_NEXT2_OFFEST;
		closePreventRevokeParam = (DWORD)hWeChatWin + ADDR_CLOSEPREVENTREVOKE_PARAM_OFFEST;

		preventRevokeAddr3 = (DWORD)hWeChatWin + ADDR_OPENPREVENTREVOKE_ADDR3_OFFEST;


		DWORD addrHookMachineCode = preventRevokeParam - closePreventRevokeNextAddr;

		BYTE machineCode[5] = { 0 };
		machineCode[0] = 0xe8;
		CHAR* addrTempHookMachineCode = (CHAR*)&addrHookMachineCode;
		machineCode[1] = *(addrTempHookMachineCode + 0);
		machineCode[2] = *(addrTempHookMachineCode + 1);
		machineCode[3] = *(addrTempHookMachineCode + 2);
		machineCode[4] = *(addrTempHookMachineCode + 3);

		BOOL flag = WriteProcessMemory((HANDLE)-1, (LPVOID) * &preventRevokeAddr1, machineCode, 5, NULL);

		//======================================================================

		DWORD addrHookMachineCode2 = closePreventRevokeParam - preventRevokeAddr2 - 5;
		BYTE machineCode2[5] = { 0 };
		machineCode2[0] = 0xe9;
		CHAR* addrTempHookMachineCode2 = (CHAR*)&addrHookMachineCode2;
		machineCode2[1] = *(addrTempHookMachineCode2 + 0);
		machineCode2[2] = *(addrTempHookMachineCode2 + 1);
		machineCode2[3] = *(addrTempHookMachineCode2 + 2);
		machineCode2[4] = *(addrTempHookMachineCode2 + 3);

		BOOL flag2 = WriteProcessMemory((HANDLE)-1, (LPVOID) * &preventRevokeAddr2, machineCode2, 5, NULL);

		//======================================================================

		BYTE machineCode3[7] = { 0x8D, 0x45, 0xD8, 0x50, 0x8D, 0x4D, 0xAC };

		BOOL flag3 = WriteProcessMemory((HANDLE)-1, (LPVOID) * &preventRevokeAddr3, machineCode3, 7, NULL);

		if (flag && flag2 && flag3) {
			isPreventRevoke = 0;
			return 1;
		}
		else {
			return 0;
		}


	}
	return 1;


}









//转发撤回消息 开始
void getForWardMsg(DWORD* msg) {

}

void __declspec(naked) _TempStroage3() {
	__asm{
		pushad
		pushfd
		push ebx
		call getForWardMsg
		add esp, 0x4
		popfd
		popad
	}

}


bool Wx_ForWardRevokeMsg() {

	if (!isOpenForwardRevokeMsg) {

		forwardRevokeMsgAddr = (DWORD)hWeChatWin + ADDR_FORWARDREVOKEMSG_ADDR_OFFEST;
		forwardRevokeMsgNextAddr = (DWORD)hWeChatWin + ADDR_FORWARDREVOKEMSG_NEXT_OFFEST;
		forwardRevokeMsgParam = (DWORD)hWeChatWin + ADDR_FORWADRREVOKEMSG_PARAM_OFFEST;

		DWORD addrHookMachineCode = (DWORD)_TempStroage3 - forwardRevokeMsgNextAddr;

		BYTE machineCode[5];
		machineCode[0] = 0xe9;
		CHAR* addrTempHookMachineCode = (CHAR*)&addrHookMachineCode;
		machineCode[1] = *(addrTempHookMachineCode + 0);
		machineCode[2] = *(addrTempHookMachineCode + 1);
		machineCode[3] = *(addrTempHookMachineCode + 2);
		machineCode[4] = *(addrTempHookMachineCode + 3);

		BOOL flag = WriteProcessMemory((HANDLE)-1, (LPVOID) * &forwardRevokeMsgAddr, machineCode, 5, NULL);

		if (flag) {
			isOpenForwardRevokeMsg = 1;
			return isOpenForwardRevokeMsg;
		}
		else {
			return isOpenForwardRevokeMsg;
		}


	}
	return isOpenForwardRevokeMsg;

}
//转发撤回消息 结束



bool Wx_IsLogin() {
	DWORD temp = (DWORD)hWeChatWin + ADDR_WXISLOGIN_OFFEST
	BYTE isLogin = (BYTE)*(DWORD*)temp;
	return isLogin;
}

void Wx_GetMoney(TCHAR* transId, TCHAR* payee) {

	PrintFunAddr((DWORD)Wx_GetMoney);

	getMoneyCall1 = (DWORD)hWeChatWin + ADDR_GETMONEY_CALL1_OFFEST;
	getMoneyCall2 = (DWORD)hWeChatWin + ADDR_GETMONEY_CALL2_OFFEST;

	WxStr* getMoneyBuff = new WxStr[2];
	getMoneyBuff[0].str = transId;
	getMoneyBuff[0].countLen();

	getMoneyBuff[1].str = payee;
	getMoneyBuff[1].countLen();

	__asm {

		sub esp, 0x30
		mov ecx, esp
		mov eax, getMoneyBuff
		push eax
		call [getMoneyCall1]
		call [getMoneyCall2]
		add esp, 0x30

	}

}



void Wx_AttenPublicUser(TCHAR* publicUser) {

	PrintFunAddr((DWORD)Wx_AttenPublicUser);

	attenPublicUserCall1 = (DWORD)hWeChatWin + ADDR_ATTENPUBLICUSER_CALL1_OFFEST;
	attenPublicUserCall2 = (DWORD)hWeChatWin + ADDR_ATTENPUBLICUSER_CALL2_OFFEST;
	attenPublicUserParam = (DWORD)hWeChatWin + ADDR_ATTENPUBLICUSER_PARAM_OFFEST;

	attenPublicUserParam1 = (DWORD)hWeChatWin + ADDR_ATTENPUBLICUSER_PARAM1_OFFEST;
	attenPublicUserParam2 = (DWORD)hWeChatWin + ADDR_ATTENPUBLICUSER_PARAM2_OFFEST;
	attenPublicUserCall3 = (DWORD)hWeChatWin + ADDR_ATTENPUBLICUSER_CALL3_OFFEST;
	attenPublicUserCall4 = (DWORD)hWeChatWin + ADDR_ATTENPUBLICUSER_CALL4_OFFEST;

	WxStr wxPublicUser;
	wxPublicUser.str = publicUser;
	wxPublicUser.countLen();

	__asm {

		mov eax, [attenPublicUserParam1]
		sub esp, 0x18
		mov ecx, esp
		mov dword ptr ss : [ebp + 0x1C] , esp
		push eax
		call [attenPublicUserCall3]
		push 0x11
		sub esp, 0x14
		mov ecx, esp
		mov dword ptr ss : [ebp + 0x18] , esp
		push - 0x1
		mov dword ptr ds : [ecx] , 0x0
		mov dword ptr ds : [ecx + 0x4] , 0x0
		mov dword ptr ds : [ecx + 0x8] , 0x0
		push [attenPublicUserParam2]
		mov dword ptr ds : [ecx + 0xC] , 0x0
		mov dword ptr ds : [ecx + 0x10] , 0x0
		call [attenPublicUserCall4]

	}

	__asm {
		lea edi, wxPublicUser
		push 0x1
		sub esp, 0x14
		mov ecx, esp
		mov dword ptr ss : [ebp + 0x8] , esp
		push - 0x1
		mov dword ptr ds : [ecx] , 0x0
		mov dword ptr ds : [ecx + 0x4] , 0x0
		mov dword ptr ds : [ecx + 0x8] , 0x0
		mov dword ptr ds : [ecx + 0xC] , 0x0
		mov dword ptr ds : [ecx + 0x10] , 0x0
		push dword ptr ds : [edi]
		call [attenPublicUserCall1]
		mov eax, attenPublicUserParam
		mov eax, [eax]
		mov ecx, eax
		call [attenPublicUserCall2]
	}

}