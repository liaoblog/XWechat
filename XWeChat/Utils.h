#include "stdafx.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"
#include "include/rapidjson/document.h"

using namespace rapidjson;
typedef GenericDocument<UTF16<> > WDocument;
typedef GenericValue<UTF16<> > WValue;

void PrintFunAddr(DWORD addr);
int ByteCmp(CHAR* buff, DWORD len);
void SendError(TCHAR* data, TCHAR* content, TCHAR* msgId = 0);
void SendSuccess(TCHAR* data, TCHAR* content, TCHAR* msgId = 0);

struct WxDataJsonParse {

public:
	WDocument doc;
	WValue val;
	TCHAR* buff;
	TCHAR* typeValue;
	WValue contentValue;
	WValue wmsgId;
	TCHAR* msgId = 0;
	bool parse();

	bool parseType();

	bool parseContent();

	~WxDataJsonParse();

};


struct WxErrDataJsonBuild {

	StringBuffer s;
	Writer<StringBuffer, UTF16<>>* writer = new Writer<StringBuffer, UTF16<>>(s);

	WxErrDataJsonBuild(TCHAR* data, TCHAR* content);
	~WxErrDataJsonBuild();

};

struct WxSuccDataJsonBuild {

	StringBuffer s;
	Writer<StringBuffer, UTF16<>>* writer = new Writer<StringBuffer, UTF16<>>(s);

	WxSuccDataJsonBuild(TCHAR* data, TCHAR* content);
	~WxSuccDataJsonBuild();

};