#pragma once
#include <Windows.h>
#include<iostream>
#include "Wx_AddrOffest.h"
#include <WinSock2.h>
#include <map>
//声明全局数据

struct Wx_jsonBuff {
	std::string str;
	DWORD len;

	void Getstr(TCHAR* msgId);
};
struct WxSocket {

public:
	SOCKADDR_IN server_addr;
	SOCKET s_server;

public:
	WxSocket();
	~WxSocket();

public:
	int wxConnect();
	void wxRecv(void(*callback)(CHAR* recvBuff));
	void wxSend(CHAR* buff, DWORD len);
	void wxSend(Wx_jsonBuff s);

};

struct WxOwnInfo {

	TCHAR* country;
	DWORD countryLen;

	TCHAR* city;
	DWORD cityLen;

	TCHAR* provice;
	DWORD proviceLen;

	TCHAR* wxId;
	DWORD wxIdLen;

	TCHAR* wxPhoneNumber;
	DWORD wxPhoneNumberLen;

	SHORT sex;

	TCHAR* wxNumber;
	DWORD wxNumberLen;

	TCHAR* loginDevice;
	DWORD loginDeviceLen;

	TCHAR* nickName;
	DWORD nickNameLen;

	TCHAR* headImg;
	DWORD headImgLen;

	TCHAR* signature;
	DWORD signatureLen;

	TCHAR* fileSavePath;
	DWORD fileSavePathLen;

	WxOwnInfo();
	void init();
	~WxOwnInfo();

};

struct WxStr {

	TCHAR* str;
	DWORD len;
	DWORD maxLen;
private:
	DWORD zero;
	DWORD zero2;
public:
	WxStr();
	void countLen();

};

struct WxAddrSct {

	DWORD startAddr;
	DWORD endAddr;
	DWORD endAddr2;

	WxAddrSct();

};

struct WxFileStr {

	DWORD msgType;
	TCHAR* str;
	DWORD len;
	DWORD maxLen;
private:
	DWORD zero;
	DWORD zero2;
	DWORD zero3;
	DWORD zero4;
	DWORD zero5;
public:
	WxFileStr();
	void countLen();
};

struct WxUserInfo {

	TCHAR* wxid;
	TCHAR* wxNumber;
	DWORD wxNumberLen;
	TCHAR* v1;
	DWORD v1Len;
	TCHAR* nickName;
	TCHAR* remark;
	DWORD remarkLen;
	//TCHAR* nickNameByFirstLetterUpper;
	//TCHAR* nickNameBySpell;

};

struct WxChatRoomInfo {

	TCHAR* chatRoomId;

	TCHAR* memberList;

	TCHAR* memberNickNameList;
	DWORD memberNickNameListLen;

	TCHAR* chatRoomAdmin;

	TCHAR* ownNickName;
	DWORD ownNickNameLen;

	DWORD memberNum;

};

struct  WxArticleInfo{

	TCHAR* title;
	TCHAR* desc;
	TCHAR* url;
	TCHAR* imgUrl;

};

struct WxSmallAppInfo {
	TCHAR* appName;
	TCHAR* title;
	TCHAR* appId;
	TCHAR* url;
	TCHAR* pagePath;
	TCHAR* userName;
	TCHAR* weAppIconUrl;
};



extern bool isPrint;
extern HANDLE hWeChatWin;
extern DWORD pFriendListHeadPointer;
extern WxSocket sctSocket;
extern WxOwnInfo wxOwnInfo;
extern CHAR* preventRevokeTip;
extern DWORD isOpenGetMessage;     //是否打开监控消息
extern DWORD isPreventRevoke;      //是否打开防撤回
extern DWORD isOpenForwardRevokeMsg;  //是否打开转发撤回消息
extern std::map<std::wstring, DWORD> databaseMap;
