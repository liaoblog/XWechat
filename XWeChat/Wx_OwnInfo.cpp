#include "stdafx.h"
#include "Wx_GlobalData.h"
#include <Windows.h>
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"
#include "Utils.h"

using namespace rapidjson;

static DWORD getWXFilePathCall;

void CheckData(DWORD* data, DWORD maxLen, DWORD type);
void GetWXFilePath(WxStr* buff);

void Wx_SetWxOwnInfo() {

	wxOwnInfo.~WxOwnInfo();

	DWORD temp = (DWORD)hWeChatWin + ADDR_OWNINFO_COUNTRY_OFFEST;
	DWORD tempLen = 0x10 + (DWORD)hWeChatWin + ADDR_OWNINFO_COUNTRY_OFFEST;
	DWORD tempMaxLen = 0x14 + (DWORD)hWeChatWin + ADDR_OWNINFO_COUNTRY_OFFEST;
	CheckData(&temp, *(DWORD*)tempMaxLen, 1);
	//wxOwnInfo.country = (TCHAR*)temp;
	wxOwnInfo.countryLen = wcslen(wxOwnInfo.country);

	temp = (DWORD)hWeChatWin + ADDR_OWNINFO_CITY_OFFEST;
	tempLen = 0x10 + (DWORD)hWeChatWin + ADDR_OWNINFO_CITY_OFFEST;
	tempMaxLen = 0x14 + (DWORD)hWeChatWin + ADDR_OWNINFO_CITY_OFFEST;
	CheckData(&temp, *(DWORD*)tempMaxLen, 2);
	//wxOwnInfo.city = (TCHAR*)temp;
	wxOwnInfo.cityLen = wcslen(wxOwnInfo.city);

	temp = (DWORD)hWeChatWin + ADDR_OWNINFO_SEX_OFFEST;
	wxOwnInfo.sex = *(SHORT*)temp;


	temp = (DWORD)hWeChatWin + ADDR_OWNINFO_PROVINCE_OFFEST;
	tempLen = 0x10 + (DWORD)hWeChatWin + ADDR_OWNINFO_PROVINCE_OFFEST;
	tempMaxLen = 0x14 + (DWORD)hWeChatWin + ADDR_OWNINFO_PROVINCE_OFFEST;
	CheckData(&temp, *(DWORD*)tempMaxLen, 3);
	//wxOwnInfo.provice = (TCHAR*)temp;
	wxOwnInfo.proviceLen = wcslen(wxOwnInfo.provice);

	temp = (DWORD)hWeChatWin + ADDR_OWNINFO_WXID_OFFEST;
	tempLen =0x10+ (DWORD)hWeChatWin + ADDR_OWNINFO_WXID_OFFEST;
	tempMaxLen = 0x14 + (DWORD)hWeChatWin + ADDR_OWNINFO_WXID_OFFEST;
	CheckData(&temp, *(DWORD*)tempMaxLen, 4);
	//wxOwnInfo.wxId = (TCHAR*)temp;
	wxOwnInfo.wxIdLen = wcslen(wxOwnInfo.wxId);

	temp = (DWORD)hWeChatWin + ADDR_OWNINFO_WXNUMBER_OFFEST;
	tempLen =0x10+ (DWORD)hWeChatWin + ADDR_OWNINFO_WXNUMBER_OFFEST;
	tempMaxLen = 0x14 + (DWORD)hWeChatWin + ADDR_OWNINFO_WXNUMBER_OFFEST;
	CheckData(&temp, *(DWORD*)tempMaxLen, 5);
	//wxOwnInfo.wxNumber = (TCHAR*)temp;
	wxOwnInfo.wxNumberLen = wcslen(wxOwnInfo.wxNumber);

	temp = (DWORD)hWeChatWin + ADDR_OWNINFO_PHONENUMBER_OFFEST;
	tempLen =0x10+ (DWORD)hWeChatWin + ADDR_OWNINFO_PHONENUMBER_OFFEST;
	tempMaxLen = 0x14 + (DWORD)hWeChatWin + ADDR_OWNINFO_PHONENUMBER_OFFEST;
	CheckData(&temp, *(DWORD*)tempMaxLen, 6);
	//wxOwnInfo.wxPhoneNumber = (TCHAR*)temp;
	wxOwnInfo.wxPhoneNumberLen = wcslen(wxOwnInfo.wxPhoneNumber);

	temp = (DWORD)hWeChatWin + ADDR_OWNINFO_LOGINDEVICE_OFFEST;
	tempLen =0x10+ (DWORD)hWeChatWin + ADDR_OWNINFO_LOGINDEVICE_OFFEST;
	tempMaxLen = 0x14 + (DWORD)hWeChatWin + ADDR_OWNINFO_LOGINDEVICE_OFFEST;
	CheckData(&temp, *(DWORD*)tempMaxLen, 7);
	//wxOwnInfo.loginDevice = (TCHAR*)temp;
	wxOwnInfo.loginDeviceLen = wcslen(wxOwnInfo.loginDevice);

	temp = (DWORD)hWeChatWin + ADDR_OWNINFO_NICKNAME_OFFEST;
	tempLen =0x10+ (DWORD)hWeChatWin + ADDR_OWNINFO_NICKNAME_OFFEST;
	tempMaxLen = 0x14 + (DWORD)hWeChatWin + ADDR_OWNINFO_NICKNAME_OFFEST;
	CheckData(&temp, *(DWORD*)tempMaxLen, 8);
	//wxOwnInfo.nickName = (TCHAR*)temp;
	wxOwnInfo.nickNameLen = wcslen(wxOwnInfo.nickName);

	temp = (DWORD)hWeChatWin + ADDR_OWNINFO_HEADIMG_OFFEST;
	tempLen =0x10+ (DWORD)hWeChatWin + ADDR_OWNINFO_HEADIMG_OFFEST;
	tempMaxLen = 0x14 + (DWORD)hWeChatWin + ADDR_OWNINFO_HEADIMG_OFFEST;
	CheckData(&temp, *(DWORD*)tempMaxLen, 9);
	//wxOwnInfo.headImg = (TCHAR*)temp;
	wxOwnInfo.headImgLen = wcslen(wxOwnInfo.headImg);

	temp = (DWORD)hWeChatWin + ADDR_OWNINFO_SIGNATURE_OFFEST;
	tempLen =0x10+ (DWORD)hWeChatWin + ADDR_OWNINFO_SIGNATURE_OFFEST;
	tempMaxLen = 0x14 + (DWORD)hWeChatWin + ADDR_OWNINFO_SIGNATURE_OFFEST;
	CheckData(&temp, *(DWORD*)tempMaxLen, 10);
	//wxOwnInfo.signature = (TCHAR*)temp;
	wxOwnInfo.signatureLen = wcslen(wxOwnInfo.signature);

	WxStr wxFilePathBuff;
	GetWXFilePath(&wxFilePathBuff);
	CHAR* tempPathBuff = new CHAR[wxFilePathBuff.len * 2 + 2];
	memcpy((DWORD*)tempPathBuff, (DWORD*)wxFilePathBuff.str, wxFilePathBuff.len * 2 + 2);
	wxOwnInfo.fileSavePath = (TCHAR*)tempPathBuff;
	wxOwnInfo.fileSavePathLen = wxFilePathBuff.len;

}


Wx_jsonBuff Wx_GetWxOwnInfo(bool isSend = true) {

	PrintFunAddr((DWORD)Wx_GetWxOwnInfo);
	PrintFunAddr((DWORD)&wxOwnInfo);

	Wx_SetWxOwnInfo();


	if (isSend) {

		StringBuffer s;
		Writer<StringBuffer, UTF16<>> writer(s);

		writer.StartObject();

		writer.Key(L"type");
		writer.String(L"Wx_OwnInfo");

		writer.Key(L"status");
		writer.String(L"success");

		writer.Key(L"wxid");
		writer.String(wxOwnInfo.wxId);

		writer.Key(L"content");
		writer.StartObject();

		writer.Key(L"country");
		writer.String(wxOwnInfo.country);

		writer.Key(L"city");
		writer.String(wxOwnInfo.city);

		writer.Key(L"provice");
		writer.String(wxOwnInfo.provice);

		writer.Key(L"wxid");
		writer.String(wxOwnInfo.wxId);

		writer.Key(L"wxNumber");
		writer.String(wxOwnInfo.wxNumber);

		writer.Key(L"wxPhoneNumber");
		writer.String(wxOwnInfo.wxPhoneNumber);

		writer.Key(L"loginDevice");
		writer.String(wxOwnInfo.loginDevice);

		writer.Key(L"nickName");
		writer.String(wxOwnInfo.nickName);

		writer.Key(L"signature");
		writer.String(wxOwnInfo.signature);

		writer.Key(L"headImg");
		writer.String(wxOwnInfo.headImg);

		writer.Key(L"sex");
		writer.Int(wxOwnInfo.sex);

		writer.Key(L"fileSavePath");
		writer.String(wxOwnInfo.fileSavePath);

		writer.EndObject();
		writer.EndObject();

		//PrintFunAddr(s.GetSize());
		Wx_jsonBuff jb;
		jb.str = (CHAR*)s.GetString();
		jb.len = s.GetSize();
		return jb;
		//sctSocket.wxSend((CHAR*)s.GetString(), s.GetSize());

	}
	Wx_jsonBuff jb;
	return jb;

}

void GetWXFilePath(WxStr* buff) {

	getWXFilePathCall = (DWORD)hWeChatWin + ADDR_GETWXFILEPATH_CALL_OFFEST;
	
	__asm {

		mov ecx, buff
		call [getWXFilePathCall]

	}

}

void CheckData(DWORD* data, DWORD maxLen, DWORD type) {

	if (maxLen > 0xF) {

		DWORD temp = *data;
		*data = *(DWORD*)temp;

	}
	
	DWORD buffSize = MultiByteToWideChar(CP_UTF8, 0, (LPCCH)*data, -1, NULL, 0);

	if (buffSize > 0) {
		//TCHAR* buff = new TCHAR[buffSize];
		TCHAR* buff = (TCHAR*)malloc(buffSize * 2);
		MultiByteToWideChar(CP_UTF8, 0, (LPCCH)*data, -1, buff, buffSize);
		switch (type)
		{
		case 1:
			wxOwnInfo.country = buff;
			break;
		case 2:
			wxOwnInfo.city = buff;
			break;
		case 3:
			wxOwnInfo.provice = buff;
			break;
		case 4:
			wxOwnInfo.wxId = buff;
			break;
		case 5:
			wxOwnInfo.wxNumber = buff;
			break;
		case 6:
			wxOwnInfo.wxPhoneNumber = buff;
			break;
		case 7:
			wxOwnInfo.loginDevice = buff;
			break;
		case 8:
			wxOwnInfo.nickName = buff;
			break;
		case 9:
			wxOwnInfo.headImg = buff;
			break;
		case 10:
			wxOwnInfo.signature = buff;
			break;
		default:
			break;
		}
	}
	
	//memcpy((DWORD*)*data, (DWORD*)buff, buffSize * 2);

}

//data > temp > hw > p

