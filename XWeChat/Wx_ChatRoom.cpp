#pragma once
#include "stdafx.h"
#include "Utils.h"
#include "Wx_GlobalData.h"
#include "Wx_AddrOffest.h"
#include "Wx_Utils.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"

using namespace rapidjson;

static DWORD updateChatRoomPostCall;

static DWORD inviteFriendJoinChatRoomCall1;
static DWORD inviteFriendJoinChatRoomCall2;
static DWORD inviteFriendJoinChatRoomParam;

static DWORD createChatRoomCall;
static DWORD createChatRoomParam;

static DWORD kickFriendCall1;
static DWORD kickFriendCall2;
static DWORD kickFriendCall3;

static DWORD quitChatRoomCall1;
static DWORD quitChatRoomCall2;
static DWORD quitChatRoomCall3;
static DWORD quitChatRoomCall4;

static DWORD updateChatRoomName;

static DWORD updateChatRoomNickNameCall1;
static DWORD updateChatRoomNickNameCall2;

static DWORD getChatRoomInfoCall1;
static DWORD getChatRoomInfoCall2;
static DWORD getChatRoomInfoCall3;

void GetChatRoomInfo(WxStr* chatRoomId, CHAR* buff);
void MsgPack(WxChatRoomInfo* chatRoomInfo, Wx_jsonBuff* jb);

void Wx_UpdateChatRoomPost(TCHAR* chatRoomId, TCHAR* text) {

	PrintFunAddr((DWORD)Wx_UpdateChatRoomPost);

	updateChatRoomPostCall = (DWORD)hWeChatWin + ADDR_UPDATECHATROOMPOST_CALL_OFFEST;

	WxAddrSct buff;

	CHAR postInfoBuff[0x60] = { 0 };

	*(DWORD*)postInfoBuff = (DWORD)chatRoomId;
	*(DWORD*)(postInfoBuff + 0x4) = wcslen(chatRoomId);
	*(DWORD*)(postInfoBuff + 0x8) = wcslen(chatRoomId) * 2;

	*(DWORD*)(postInfoBuff + 0x14) = (DWORD)text;
	*(DWORD*)(postInfoBuff + 0x18) = wcslen(text);
	*(DWORD*)(postInfoBuff + 0x1c) = wcslen(text) * 2;

	*(DWORD*)(postInfoBuff + 0x28) = (DWORD)L"BLOG";
	*(DWORD*)(postInfoBuff + 0x2c) = 13;
	*(DWORD*)(postInfoBuff + 0x30) = 20;

	buff.startAddr = (DWORD)postInfoBuff;
	buff.endAddr = (DWORD)postInfoBuff + 0x60;
	buff.endAddr2 = buff.endAddr;

	__asm {

		lea eax, buff
		push eax
		call [updateChatRoomPostCall]


	}

}

void Wx_CreateChatRoom(DWORD* inviteeWxIdListAddr, DWORD inviteeWxIdListLen) {

	PrintFunAddr((DWORD)Wx_CreateChatRoom);
	
	createChatRoomCall = (DWORD)hWeChatWin + ADDR_CREATECHATROOM_CALL_OFFEST;
	createChatRoomParam = (DWORD)hWeChatWin + ADDR_CREATECHATROOM_PARAM_OFFEST;

	WxStr* wxInviteeWxIdList = new WxStr[inviteeWxIdListLen];

	WxAddrSct wxInviteeWxIdBuff;

	Wx_WxIdListPack(wxInviteeWxIdList, inviteeWxIdListLen, inviteeWxIdListAddr, &wxInviteeWxIdBuff);

	__asm {

		sub esp, 0x14
		mov ecx, [createChatRoomParam]
		lea esi, wxInviteeWxIdBuff
		push esi
		call [createChatRoomCall]

	}

	delete[] wxInviteeWxIdList;
	delete[] inviteeWxIdListAddr;
}

void Wx_InviteFriendJoinChatRoom(TCHAR* chatRoomId, DWORD* inviteeWxIdListAddr, DWORD inviteeWxIdListLen) {

	PrintFunAddr((DWORD)Wx_InviteFriendJoinChatRoom);

	inviteFriendJoinChatRoomCall1 = (DWORD)hWeChatWin + ADDR_INVITEFRIENDJOINCHATROOM_CALL1_OFFEST;
	inviteFriendJoinChatRoomCall2 = (DWORD)hWeChatWin + ADDR_INVITEFRIENDJOINCHATROOM_CALL2_OFFEST;
	inviteFriendJoinChatRoomParam = (DWORD)hWeChatWin + ADDR_INVITEFRIENDJOINCHATROOM_PARAM_OFFEST;

	CHAR chatRoomBuff[0x1DC] = { 0 };
	*(DWORD*)(chatRoomBuff + 0x8) = (DWORD)chatRoomId;
	*(DWORD*)(chatRoomBuff + 0xc) = wcslen(chatRoomId);
	*(DWORD*)(chatRoomBuff + 0x10) = wcslen(chatRoomId) * 2;

	WxStr* wxInviteeWxIdList = new WxStr[inviteeWxIdListLen];

	WxAddrSct wxInviteeWxIdBuff;

	Wx_WxIdListPack(wxInviteeWxIdList, inviteeWxIdListLen, inviteeWxIdListAddr, &wxInviteeWxIdBuff);

	WxStr wxChatRoomBuff;

	__asm {

		sub esp, 0x14
		lea ecx, chatRoomBuff
		mov eax, esp
		mov dword ptr ss : [ebp - 0x18] , esp
		push eax
		call [inviteFriendJoinChatRoomCall1]

		lea esi, wxInviteeWxIdBuff
		push esi
		mov ecx, [inviteFriendJoinChatRoomParam]
		call [inviteFriendJoinChatRoomCall2]

	}

	delete[] wxInviteeWxIdList;
	delete[] inviteeWxIdListAddr;
}

void Wx_KickFriendInChatRoom(TCHAR* chatRoomId, DWORD* kickedWxIdListAddr, DWORD kickedWxIdListLen) {

	PrintFunAddr((DWORD)Wx_KickFriendInChatRoom);

	kickFriendCall1 = (DWORD)hWeChatWin + ADDR_KICKFRIENDINCHATROOM_CALL1_OFFEST;
	kickFriendCall2 = (DWORD)hWeChatWin + ADDR_KICKFRIENDINCHATROOM_CALL2_OFFEST;
	kickFriendCall3 = (DWORD)hWeChatWin + ADDR_KICKFRIENDINCHATROOM_CALL3_OFFEST;

	WxStr wxChatRoomId;
	wxChatRoomId.str = chatRoomId;
	wxChatRoomId.len = wcslen(chatRoomId);
	wxChatRoomId.maxLen = wxChatRoomId.len * 2;

	WxStr* wxKickedWxIdList = new WxStr[kickedWxIdListLen];

	WxAddrSct wxKickedWxIdBuff;

	Wx_WxIdListPack(wxKickedWxIdList, kickedWxIdListLen, kickedWxIdListAddr, &wxKickedWxIdBuff);

	__asm {

		sub esp, 0x14
		lea eax, wxChatRoomId
		mov ecx, esp
		mov dword ptr ss : [ebp - 0x1A8] , esp
		lea edi, wxKickedWxIdBuff
		push eax
		call [kickFriendCall1]
		push edi
		call [kickFriendCall2]
		mov ecx, eax
		call [kickFriendCall3]

	}

	delete[] wxKickedWxIdList;

}

void Wx_QuitChatRoom(TCHAR* chatRoomId) {

	PrintFunAddr((DWORD)Wx_CreateChatRoom);

	quitChatRoomCall1 = (DWORD)hWeChatWin + ADDR_QUITCHATROOM_CALL1_OFFEST;
	quitChatRoomCall2 = (DWORD)hWeChatWin + ADDR_QUITCHATROOM_CALL2_OFFEST;
	quitChatRoomCall3 = (DWORD)hWeChatWin + ADDR_QUITCHATROOM_CALL3_OFFEST;
	quitChatRoomCall4 = (DWORD)hWeChatWin + ADDR_QUITCHATROOM_CALL4_OFFEST;

	WxStr chatRoomBuff;
	chatRoomBuff.str = chatRoomId;
	chatRoomBuff.len = wcslen(chatRoomId);
	chatRoomBuff.maxLen = chatRoomBuff.len * 2;

	__asm {

		push 0x58
		call [quitChatRoomCall1]
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x13C] , eax
		lea ecx, chatRoomBuff
		push ecx
		mov ecx, eax
		call [quitChatRoomCall2]
		push eax
		push 0x0
		push 0x0
		push 0x317
		call [quitChatRoomCall3]
		mov ecx, eax
		call [quitChatRoomCall4]

	}

}

WxChatRoomInfo Wx_GetChatRoomInfo(TCHAR* chatRoomId, Wx_jsonBuff* jb, bool isSend = true) {


	WxChatRoomInfo(*pWx_GetChatRoomInfo)(TCHAR*, Wx_jsonBuff * , bool);
	pWx_GetChatRoomInfo = Wx_GetChatRoomInfo;
	PrintFunAddr((DWORD)pWx_GetChatRoomInfo);

	WxStr wxChatRoomIdBuff;
	wxChatRoomIdBuff.str = chatRoomId;
	wxChatRoomIdBuff.countLen();

	CHAR chatRoomInfoBuff[0x150] = { 0 };

	GetChatRoomInfo(&wxChatRoomIdBuff, chatRoomInfoBuff);

	WxChatRoomInfo chatRoomInfo = { 0 };

	chatRoomInfo.chatRoomId = chatRoomId;

	chatRoomInfo.chatRoomAdmin = (TCHAR*) * ((DWORD*)(chatRoomInfoBuff + 0x44));

	chatRoomInfo.memberList = (TCHAR*) * ((DWORD*)(chatRoomInfoBuff + 0x18));

	if (*((DWORD*)(chatRoomInfoBuff + 0x2c)) > 0) {
		chatRoomInfo.memberNickNameList = (TCHAR*) * ((DWORD*)(chatRoomInfoBuff + 0x2c));
	}
	else {
		chatRoomInfo.memberNickNameList = (TCHAR*)L"";
	}
	chatRoomInfo.memberNickNameListLen = * ((DWORD*)(chatRoomInfoBuff + 0x30));

	if (*((DWORD*)(chatRoomInfoBuff + 0x5c)) > 0) {
		chatRoomInfo.ownNickName = (TCHAR*) * ((DWORD*)(chatRoomInfoBuff + 0x5c));
	}
	else {
		chatRoomInfo.ownNickName = (TCHAR*)L"";
	}
	chatRoomInfo.ownNickNameLen = * ((DWORD*)(chatRoomInfoBuff + 0x60));

	chatRoomInfo.memberNum =  * ((DWORD*)(chatRoomInfoBuff + 0x94));
	
	if (isSend) {
		MsgPack(&chatRoomInfo, jb);
	}

	return chatRoomInfo;

}

void Wx_GetChatRoomInfo(TCHAR* chatRoomId, CHAR* chatRoomInfoBuff) {

	WxStr wxChatRoomIdBuff;
	wxChatRoomIdBuff.str = chatRoomId;
	wxChatRoomIdBuff.countLen();
	
	GetChatRoomInfo(&wxChatRoomIdBuff, chatRoomInfoBuff);

}

void MsgPack(WxChatRoomInfo* chatRoomInfo, Wx_jsonBuff* jb) {

	StringBuffer s;
	Writer<StringBuffer, UTF16<>> writer(s);

	writer.StartObject();

	writer.Key(L"type");
	writer.String(L"Wx_GetChatRoomInfo");

	writer.Key(L"status");
	writer.String(L"success");

	writer.Key(L"wxid");
	writer.String(wxOwnInfo.wxId);

	writer.Key(L"content");

	writer.StartObject();

	writer.Key(L"chatRoomId");
	writer.String(chatRoomInfo->chatRoomId);

	writer.Key(L"chatRoomAdmin");
	writer.String(chatRoomInfo->chatRoomAdmin);

	writer.Key(L"memberList");
	writer.String(chatRoomInfo->memberList);

	writer.Key(L"memberNickNameList");
	writer.String(chatRoomInfo->memberNickNameList);

	writer.Key(L"ownNickName");
	writer.String(chatRoomInfo->ownNickName);

	writer.Key(L"memberNum");
	writer.Int(chatRoomInfo->memberNum);

	writer.EndObject();

	writer.EndObject();

	jb->str = std::string(s.GetString());
	jb->len = s.GetSize();
	//sctSocket.wxSend((CHAR*)s.GetString(), s.GetSize());

}

void GetChatRoomInfo(WxStr* chatRoomId, CHAR* buff) {
	
	getChatRoomInfoCall1 = (DWORD)hWeChatWin + ADDR_GETCHATROOMINFO_CALL1_OFFEST;
	getChatRoomInfoCall2 = (DWORD)hWeChatWin + ADDR_GETCHATROOMINFO_CALL2_OFFEST;
	getChatRoomInfoCall3 = (DWORD)hWeChatWin + ADDR_GETCHATROOMINFO_CALL3_OFFEST;

	__asm {
		mov ecx, buff
		call [getChatRoomInfoCall1]
		mov eax, buff
		push eax
		mov eax, chatRoomId
		push eax
		call [getChatRoomInfoCall2]
		mov ecx, eax
		call [getChatRoomInfoCall3]
	}

}

/*
	此功能有缺陷
*/
void Wx_UpdateChatRoomNickName(TCHAR* chatRoomId, TCHAR* newNickName) {

	PrintFunAddr((DWORD)Wx_UpdateChatRoomNickName);

	updateChatRoomNickNameCall1 = (DWORD)hWeChatWin + ADDR_UPDATECHATROOMNICKNAME_CALL1_OFFEST;
	updateChatRoomNickNameCall2 = (DWORD)hWeChatWin + ADDR_UPDATECHATROOMNICKNAME_CALL2_OFFEST;

	WxStr wxNewNickName;
	wxNewNickName.str = newNickName;
	wxNewNickName.countLen();

	/*
		改群昵称
	*/
	__asm {

		sub esp, 0x14
		mov ecx, esp
		mov dword ptr ss : [ebp - 0x30] , esp
		push - 0x1
		mov dword ptr ds : [ecx] , 0x0
		mov dword ptr ds : [ecx + 0x4] , 0x0
		mov dword ptr ds : [ecx + 0x8] , 0x0
		mov dword ptr ds : [ecx + 0xC] , 0x0
		mov dword ptr ds : [ecx + 0x10] , 0x0
		mov edi, newNickName
		push edi
		call [updateChatRoomNickNameCall1]

		sub esp, 0x14
		mov ecx, esp
		mov dword ptr ss : [ebp - 0x34] , esp
		push - 0x1
		push [wxOwnInfo.wxId]
		mov dword ptr ds : [ecx] , 0x0
		mov dword ptr ds : [ecx + 0x4] , 0x0
		mov dword ptr ds : [ecx + 0x8] , 0x0
		mov dword ptr ds : [ecx + 0xC] , 0x0
		mov dword ptr ds : [ecx + 0x10] , 0x0
		call [updateChatRoomNickNameCall1]

		sub esp, 0x14
		mov ecx, esp
		mov dword ptr ss : [ebp - 0x38] , esp
		push - 0x1
		mov dword ptr ds : [ecx] , 0x0
		mov dword ptr ds : [ecx + 0x4] , 0x0
		mov dword ptr ds : [ecx + 0x8] , 0x0
		mov dword ptr ds : [ecx + 0xC] , 0x0
		mov dword ptr ds : [ecx + 0x10] , 0x0
		push [chatRoomId]
		call [updateChatRoomNickNameCall1]

		lea edi, wxNewNickName
		call [updateChatRoomNickNameCall2]

	}

	/*
		获取群信息, 设置群信息到界面（不设置会有问题）
	*/
	/*
	CHAR chatRoomMemberListInfoBuff[0x150] = { 0 };

	WxStr wxChatRoomIdBuff;
	wxChatRoomIdBuff.str = chatRoomId;
	wxChatRoomIdBuff.countLen();

	__asm {
		lea ecx, chatRoomMemberListInfoBuff
		call[updateChatRoomNickNameCall3]
		lea eax, chatRoomMemberListInfoBuff
		push eax
		lea eax, wxChatRoomIdBuff
		push eax
		call[updateChatRoomNickNameCall4]
		mov ecx, eax
		call[updateChatRoomNickNameCall5]
	}

	WxAddrSct wxChatRoomMemberListInfoBuff;
	wxChatRoomMemberListInfoBuff.startAddr = (DWORD)chatRoomMemberListInfoBuff;
	wxChatRoomMemberListInfoBuff.endAddr = (DWORD)chatRoomMemberListInfoBuff + sizeof(chatRoomMemberListInfoBuff);
	wxChatRoomMemberListInfoBuff.endAddr2 = wxChatRoomMemberListInfoBuff.endAddr;

	__asm {
		mov ecx, 0
		push ecx
		lea eax, wxChatRoomMemberListInfoBuff
		push eax
		call [updateChatRoomNickNameCall4]
		mov ecx, eax
		call [updateChatRoomNickNameCall6]    
	}
	*/
}

void Wx_UpdateChatRoomName(TCHAR* chatRoomId, TCHAR* newName) {

	PrintFunAddr((DWORD)Wx_CreateChatRoom);

	updateChatRoomName = (DWORD)hWeChatWin + ADDR_UPDATECHATROOMNAME_CALL_OFFEST;

	WxStr wxNewName;
	wxNewName.str = newName;
	wxNewName.countLen();

	WxStr wxChatRoomId;
	wxChatRoomId.str = chatRoomId;
	wxChatRoomId.countLen();

	__asm {
		lea edi, wxChatRoomId
		lea edx, wxNewName
		mov ecx, edi
		call [updateChatRoomName]
	}

}