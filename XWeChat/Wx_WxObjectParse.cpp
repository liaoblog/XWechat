#include "stdafx.h"
#include "include/rapidjson/document.h"
#include "Wx_WxObjectParse.h"

using namespace rapidjson;
typedef GenericDocument<UTF16<> > WDocument;
typedef GenericValue<UTF16<> > WValue;

bool Wx_Parse_Wx_GetInfoByWxid::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				val = val[L"wxid"];
				if (val.GetStringLength() > 0) {
					wxid = (TCHAR*)val.GetString();
					return true;
				}
			}
			
		}

	}
	return false;

}

bool Wx_Parse_Wx_GetInfoByWxid::Parse(WValue content) {

	val = val[L"wxid"];
	if (val.GetStringLength() > 0) {
		wxid = (TCHAR*)val.GetString();
		return true;
	}
	return false;

}


bool Wx_Parse_Wx_DeleteFriend::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				val = val[L"wxid"];
				if (val.GetStringLength() > 0) {
					wxid = (TCHAR*)val.GetString();
					return true;
				}
			}

		}

	}
	return false;

}

bool Wx_Parse_Wx_OpenPreventRevoke::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				val = val[L"text"];
				if (val.GetStringLength() > 0) {
					text = (TCHAR*)val.GetString();
					return true;
				}
			}

		}

	}
	return false;

}

bool Wx_Parse_Wx_GetChatRoomInfo::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				val = val[L"chatroomid"];
				if (val.GetStringLength() > 0) {
					chatRoomId = (TCHAR*)val.GetString();
					return true;
				}
			}

		}

	}
	return false;

}

bool Wx_Parse_Wx_QuitChatRoom::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				val = val[L"chatroomid"];
				if (val.GetStringLength() > 0) {
					chatRoomId = (TCHAR*)val.GetString();
					return true;
				}
			}

		}

	}
	return false;

}



bool Wx_Parse_Wx_AgreeFriend::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {

				wv1 = val[L"v1"];
				wv2 = val[L"v2"];
				if (wv1.GetStringLength() > 0 && wv2.GetStringLength() > 0) {
					this->v1 = (TCHAR*)wv1.GetString();
					this->v2 = (TCHAR*)wv2.GetString();
					return true;
				}
			}

		}

	}
	return false;

}

bool Wx_Parse_Wx_UpdateChatRoomPost::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				wchatRoomId = val[L"chatroomid"];
				wtext = val[L"text"];
				if (wchatRoomId.GetStringLength() > 0) {
					this->chatRoomId = (TCHAR*)wchatRoomId.GetString();
					
					if (wtext.GetStringLength() > 0) {
						this->text = (TCHAR*)wtext.GetString();
					}
					else {
						this->text = (TCHAR*)L"";
					}
					return true;
				}
			}

		}

	}
	return false;

}

bool Wx_Parse_Wx_UpdateChatRoomName::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {

				wchatRoomId = val[L"chatroomid"];
				wnewName = val[L"newname"];
				if (wchatRoomId.GetStringLength() > 0) {
					this->chatRoomId = (TCHAR*)wchatRoomId.GetString();

					if (wnewName.GetStringLength() > 0) {
						this->newName = (TCHAR*)wnewName.GetString();
					}
					else {
						this->newName = (TCHAR*)L"";
					}
					return true;
				}
			}

		}

	}
	return false;

}

bool Wx_Parse_Wx_InviteFriendJoinChatRoom::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				wchatRoomId = val[L"chatroomid"];
				wlist = val[L"list"];
				if (wchatRoomId.GetStringLength() > 0) {
					this->chatRoomId = (TCHAR*)wchatRoomId.GetString();

					if (wlist.IsArray() && !wlist.Empty()) {

						DWORD* listAddr = new DWORD[wlist.Size()];
						TCHAR* tempData;
						for (DWORD i = 0; i < wlist.Size(); i++) {
							tempData = (TCHAR*)wlist[i].GetString();
							listAddr[i] = (DWORD)tempData;
						}

						this->inviteeWxIdListAddr = listAddr;
						this->inviteeWxIdListLen = wlist.Size();
						return true;
					}
				}
			}

		}

	}
	return false;

}


bool Wx_Parse_Wx_CreateChatRoom::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				wlist = val[L"list"];
				if (wlist.IsArray() && !wlist.Empty()) {

					DWORD* listAddr = new DWORD[wlist.Size()];
					TCHAR* tempData;
					for (DWORD i = 0; i < wlist.Size(); i++) {
						tempData = (TCHAR*)wlist[i].GetString();
						listAddr[i] = (DWORD)tempData;
					}

					this->inviteeWxIdListAddr = listAddr;
					this->inviteeWxIdListLen = wlist.Size();
					return true;
				}

			}

		}

	}
	return false;

}

bool Wx_Parse_Wx_KickFriendInChatRoom::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				wchatRoomId = val[L"chatroomid"];
				wlist = val[L"list"];
				if (wchatRoomId.GetStringLength() > 0) {
					this->chatRoomId = (TCHAR*)wchatRoomId.GetString();

					if (wlist.IsArray() && !wlist.Empty()) {

						DWORD* listAddr = new DWORD[wlist.Size()];
						TCHAR* tempData;
						for (DWORD i = 0; i < wlist.Size(); i++) {
							tempData = (TCHAR*)wlist[i].GetString();
							listAddr[i] = (DWORD)tempData;
						}

						this->inviteeWxIdListAddr = listAddr;
						this->inviteeWxIdListLen = wlist.Size();
						return true;
					}
				}
			}

		}

	}
	return false;

}

bool Wx_Parse_Wx_SendTextMessage::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				wwxid = val[L"wxid"];
				wtext = val[L"text"];
				if (wwxid.GetStringLength() > 0 && wtext.GetStringLength() > 0) {
					this->wxid = (TCHAR*)wwxid.GetString();
					this->texta = (TCHAR*)wtext.GetString();
					if (val.HasMember(L"atlist")) {
						watlist = val[L"atlist"];
						if (watlist.IsArray() && !watlist.Empty()) {

							DWORD* listAddr = new DWORD[watlist.Size()];
							TCHAR* tempData;
							for (DWORD i = 0; i < watlist.Size(); i++) {
								tempData = (TCHAR*)watlist[i].GetString();
								listAddr[i] = (DWORD)tempData;
							}

							this->atListAddr = listAddr;
							this->atListLen = watlist.Size();
							return true;
						}
					}
					else {
						this->atListAddr = 0;
						this->atListLen = 0;
						return true;
					}

				}
			}

		}

	}
	return false;

}

bool Wx_Parse_Wx_SendFriendCardMessage::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				wwxid = val[L"wxid"];
				wcardWxId = val[L"cardwxid"];
				if (wwxid.GetStringLength() > 0) {
					this->wxid = (TCHAR*)wwxid.GetString();
					
					if (wcardWxId.GetStringLength() > 0) {

						this->cardWxId = (TCHAR*)wcardWxId.GetString();
						return true;
					}
					else {
						this->cardWxId = (TCHAR*)L"";
						return true;
					}

				}
			}

		}

	}
	return false;

}

bool Wx_Parse_Wx_SendImageMessage::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				wwxid = val[L"wxid"];
				wfilepath = val[L"filepath"];
				if (wwxid.GetStringLength() > 0 && wfilepath.GetStringLength() > 0) {
					this->wxid = (TCHAR*)wwxid.GetString();
					this->filepath = (TCHAR*)wfilepath.GetString();
					return true;

				}
			}

		}

	}
	return false;

}

bool Wx_Parse_Wx_SendFileMessage::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				wwxid = val[L"wxid"];
				wfilepath = val[L"filepath"];
				if (wwxid.GetStringLength() > 0 && wfilepath.GetStringLength() > 0) {
					this->wxid = (TCHAR*)wwxid.GetString();
					this->filepath = (TCHAR*)wfilepath.GetString();
					return true;

				}
			}

		}

	}
	return false;

}

bool Wx_Parse_Wx_SendUrlMessage::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				wwxid = val[L"wxid"];
				if (wwxid.GetStringLength() > 0) {
					this->wxid = (TCHAR*)wwxid.GetString();
					val = val[L"articleinfo"];
					if (val.IsObject()) {
						wtitle = val[L"title"];
						wdesc = val[L"desc"];
						wurl = val[L"url"];
						wimgurl = val[L"imgurl"];
						this->articleInfo.title = wtitle.GetString()>0? (TCHAR*)wtitle.GetString():(TCHAR*)L"";
						this->articleInfo.desc = wdesc.GetString() > 0 ? (TCHAR*)wdesc.GetString() : (TCHAR*)L"";
						this->articleInfo.url = wurl.GetString() > 0 ? (TCHAR*)wurl.GetString() : (TCHAR*)L"";
						this->articleInfo.imgUrl = wimgurl.GetString() > 0 ? (TCHAR*)wimgurl.GetString() : (TCHAR*)L"";
						return true;
					}
					

				}
			}

		}

	}
	return false;

}

bool Wx_Parse_Wx_SendSmallAppMessage::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				wwxid = val[L"wxid"];
				if (wwxid.GetStringLength() > 0) {
					this->wxid = (TCHAR*)wwxid.GetString();
					val = val[L"smallappinfo"];
					if (val.IsObject()) {
						wtitle = val[L"title"];
						wappName = val[L"appname"];
						wurl = val[L"url"];
						wappId = val[L"appid"];
						wpagePath = val[L"pagepath"];
						wuserName = val[L"username"];
						wweAppIconUrl = val[L"weappiconurl"];
						this->smallAppInfo.title = wtitle.GetString() > 0 ? (TCHAR*)wtitle.GetString() : (TCHAR*)L"";
						this->smallAppInfo.appName = wappName.GetString() > 0 ? (TCHAR*)wappName.GetString() : (TCHAR*)L"";
						this->smallAppInfo.url = wurl.GetString() > 0 ? (TCHAR*)wurl.GetString() : (TCHAR*)L"";
						this->smallAppInfo.appId = wappId.GetString() > 0 ? (TCHAR*)wappId.GetString() : (TCHAR*)L"";
						this->smallAppInfo.pagePath = wpagePath.GetString() > 0 ? (TCHAR*)wpagePath.GetString() : (TCHAR*)L"";
						this->smallAppInfo.userName = wuserName.GetString() > 0 ? (TCHAR*)wuserName.GetString() : (TCHAR*)L"";
						this->smallAppInfo.weAppIconUrl = wweAppIconUrl.GetString() > 0 ? (TCHAR*)wweAppIconUrl.GetString() : (TCHAR*)L"";
						return true;
					}


				}
			}

		}

	}
	return false;

}

bool Wx_Parse_Wx_SendChatRoomUrlMessage::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				wwxid = val[L"wxid"];
				wchatRoomId = val[L"chatroomid"];
				if (wwxid.GetStringLength() > 0 && wchatRoomId.GetStringLength() > 0) {
					this->wxid = (TCHAR*)wwxid.GetString();
					this->chatRoomId = (TCHAR*)wchatRoomId.GetString();
					return true;

				}
			}

		}

	}
	return false;

}

bool Wx_Parse_Wx_GetMoney::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				wwxid = val[L"wxid"];
				wtransid = val[L"transid"];
				if (wwxid.GetStringLength() > 0 && wtransid.GetStringLength() > 0) {
					this->wxid = (TCHAR*)wwxid.GetString();
					this->transid = (TCHAR*)wtransid.GetString();
					return true;

				}
			}

		}

	}
	return false;

}

bool Wx_Parse_Wx_DatabaseExec::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				wdbName = val[L"dbname"];
				wsql = val[L"sql"];
				if (wdbName.GetStringLength() > 0 && wsql.GetStringLength() > 0) {
					this->dbName = (TCHAR*)wdbName.GetString();
					this->sql = (TCHAR*)wsql.GetString();
					return true;

				}
			}

		}

	}
	return false;

}


bool Wx_Parse_Wx_AddFriend::Parse(TCHAR* content) {
	doc.Parse(content);
	if (!doc.HasParseError()) {
		if (doc.HasMember(L"content")) {
			val = doc[L"content"];
			if (val.IsObject()) {
				wwxid = val[L"wxid"];
				wtext = val[L"text"];
				wtype = val[L"type"];
				if (wwxid.GetStringLength() > 0) {
					this->wxid = (TCHAR*)wwxid.GetString();
					this->text = wtext.GetString() > 0 ? (TCHAR*)wtext.GetString() : (TCHAR*)L"";
					this->type = wtype.GetInt();
					return true;

				}
			}

		}

	}
	return false;

}