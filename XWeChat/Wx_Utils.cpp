#include "stdafx.h"
#include "Wx_GlobalData.h"
#include "Utils.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"
#include "Wx_Utils.h"

using namespace rapidjson;

static DWORD getFriendListHeadPointerCall;

BinaryThreeOp::BinaryThreeOp(DWORD* headPointer) {

	writer->StartObject();

	writer->Key(L"type");
	writer->String(L"Wx_GetFriendList");

	writer->Key(L"status");
	writer->String(L"success");

	writer->Key(L"wxid");
	writer->String(wxOwnInfo.wxId);

	writer->Key(L"content");

	writer->StartObject();

	writer->Key(L"list");
	writer->StartArray();
	preTraverse(headPointer);
	writer->EndArray();

	writer->EndObject();
	writer->EndObject();
}

void BinaryThreeOp::preTraverse(DWORD* data) {
	if (*(BYTE*)((DWORD)data + 0xd) == (BYTE)0x0) {
		getData(data);
		preTraverse((DWORD*)*data);
		preTraverse((DWORD*)*(data + 2));
	}
}

void BinaryThreeOp::getData(DWORD* data) {
	writer->String((TCHAR*) * (data + 4));
}

BinaryThreeOp::~BinaryThreeOp() {
	delete writer;
}

void Wx_WxIdListPack(WxStr* wxList, DWORD len, DWORD* listAddr, WxAddrSct* wxKickedWxIdBuff) {

	unsigned int i;
	for (i = 0; i < len; i++) {

		wxList[i].str = (TCHAR*) * (listAddr + i);
		wxList[i].len = wcslen((TCHAR*) * (listAddr + i));
		wxList[i].maxLen = wcslen((TCHAR*) * (listAddr + i)) * 2;

	}

	
	wxKickedWxIdBuff->startAddr = (DWORD) & *wxList;
	wxKickedWxIdBuff->endAddr = (DWORD) & *wxList + sizeof(WxStr) * len;
	wxKickedWxIdBuff->endAddr2 = wxKickedWxIdBuff->endAddr;

}

void Wx_GetFriendListHeadPointer(DWORD* buff) {

	//PrintFunAddr((DWORD)Wx_GetFriendListHeadPointer);

	getFriendListHeadPointerCall = (DWORD)hWeChatWin + ADDR_GETFRIENDLISTHEADPOINTER_CALL_OFFEST;

	CHAR tempBuff[0x14] = { 0 };

	__asm {
		lea eax, tempBuff
		lea esi, tempBuff
		push eax
		push esi
		call [getFriendListHeadPointerCall]
		add esp, 0x8
		mov ecx, buff
		mov [ecx], eax
	}

}