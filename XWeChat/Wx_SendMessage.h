#pragma once
#include "stdafx.h"
void Wx_SendTextMessage(TCHAR* msgRecvId, TCHAR* text, DWORD* atListAddr, DWORD atListLen);
void Wx_SendFriendCardMessage(TCHAR* recvWxId, TCHAR* cardWxid);
void Wx_SendImageMessage(TCHAR* recvWxId, TCHAR* filePath);
void Wx_SendFileMessage(TCHAR* recvWxId, TCHAR* filePath);
void Wx_SendUrlMessage(TCHAR* recvWxId, WxArticleInfo articleInfo);
void Wx_SendChatRoomUrlMessage(TCHAR* chatRoomId, TCHAR* recvWxId);
void Wx_SendSmallAppMessage(TCHAR* recvWxId, WxSmallAppInfo smallAppInfo);