#pragma once
#include "stdafx.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"

using namespace rapidjson;

//����������
struct BinaryThreeOp {
	StringBuffer s;
	Writer<StringBuffer, UTF16<>>* writer = new Writer<StringBuffer, UTF16<>>(s);
	BinaryThreeOp(DWORD* headPointer);
	~BinaryThreeOp();
private:
	void preTraverse(DWORD* data);
	void getData(DWORD* data);
};

//
void Wx_WxIdListPack(WxStr* wxList, DWORD len, DWORD* listAddr, WxAddrSct* wxKickedWxIdBuff);
void Wx_GetFriendListHeadPointer(DWORD* buff);


