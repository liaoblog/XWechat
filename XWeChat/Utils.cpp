#include "stdafx.h"
#include "Wx_GlobalData.h"
#include "stdlib.h"
#include "include/rapidjson/document.h"
#include "Utils.h"

using namespace rapidjson;
typedef GenericDocument<UTF16<> > WDocument;
typedef GenericValue<UTF16<> > WValue;

void PrintFunAddr(DWORD addr) {

	if (isPrint) {
		int num = addr;
		TCHAR str[25];
		_itow_s(num, str, 16);
		MessageBox(NULL, str, 0, 0);
	}
	
}

int ByteCmp(CHAR* buff, DWORD len) {
	DWORD flag = 0;
	DWORD i;
	for (i = 0; i < len; i++) {
		if ((BYTE)*buff == (BYTE)0x7E) {
			flag = 1;
		}
		else {
			flag = 0;
		}
	}
	return flag;
}






bool WxDataJsonParse::parse() {
	doc.Parse(buff);

	if (doc.HasMember(L"msgid")) {
		wmsgId = doc[L"msgid"];
		msgId = (TCHAR*)wmsgId.GetString();
	}

	return !doc.HasParseError();
}

bool WxDataJsonParse::parseType() {
	if (doc.HasMember(L"type")) {
		val = doc[L"type"];
		if (val.GetStringLength() > 0) {
			typeValue = (TCHAR*)val.GetString();
			return true;
		}

	}

	return false;
}


bool WxDataJsonParse::parseContent() {

	if (doc.HasMember(L"content")) {
		val = doc[L"content"];
		if (val.IsObject()) {
			//contentValue = val;
			return true;
		}
		
	}

	return false;

}

WxDataJsonParse::~WxDataJsonParse() {
	//delete[] value;
}




WxErrDataJsonBuild::WxErrDataJsonBuild(TCHAR* data, TCHAR* content) {

	writer->StartObject();
	writer->Key(L"type");
	writer->String(data);
	writer->Key(L"status");
	writer->String(L"error");
	writer->Key(L"wxid");
	writer->String(wxOwnInfo.wxId);
	writer->Key(L"content");
	writer->String(content);
	writer->EndObject();

}

WxErrDataJsonBuild::~WxErrDataJsonBuild() {
	delete writer;
}

WxSuccDataJsonBuild::WxSuccDataJsonBuild(TCHAR* data, TCHAR* content) {

	writer->StartObject();
	writer->Key(L"type");
	writer->String(data);
	writer->Key(L"status");
	writer->String(L"success");
	writer->Key(L"wxid");
	writer->String(wxOwnInfo.wxId);
	writer->Key(L"content");
	writer->String(content);
	writer->EndObject();

}

WxSuccDataJsonBuild::~WxSuccDataJsonBuild() {
	delete writer;
}


void SendError(TCHAR* type, TCHAR* content, TCHAR* msgId) {

	WxErrDataJsonBuild* json = new WxErrDataJsonBuild(type, content);

	if (msgId != 0) {
		Wx_jsonBuff jb;
		jb.str = std::string(json->s.GetString());
		jb.len = json->s.GetSize();
		jb.Getstr(msgId);
		sctSocket.wxSend(jb);
	}
	else {
		sctSocket.wxSend((CHAR*)json->s.GetString(), json->s.GetSize());
	}
	

	
	delete json;

}

void SendSuccess(TCHAR* type, TCHAR* content, TCHAR* msgId) {
	WxSuccDataJsonBuild* json = new WxSuccDataJsonBuild(type, content);

	if (msgId != 0) {
		Wx_jsonBuff jb;
		jb.str = std::string(json->s.GetString());
		jb.len = json->s.GetSize();
		jb.Getstr(msgId);
		sctSocket.wxSend(jb);
	}
	else {
		sctSocket.wxSend((CHAR*)json->s.GetString(), json->s.GetSize());
	}



	delete json;
}



