#pragma once
#include "stdafx.h"
void Wx_UpdateChatRoomPost(TCHAR* chatRoomId, TCHAR* text);
void Wx_UpdateChatRoomName(TCHAR* chatRoomId, TCHAR* newName);
void Wx_UpdateChatRoomNickName(TCHAR* chatRoomId, TCHAR* newNickName);   //������
void Wx_InviteFriendJoinChatRoom(TCHAR* chatRoomId, DWORD* inviteeWxIdListAddr, DWORD inviteeWxIdListLen);
void Wx_CreateChatRoom(DWORD* inviteeWxIdListAddr, DWORD inviteeWxIdListLen);
void Wx_KickFriendInChatRoom(TCHAR* chatRoomId, DWORD* kickedWxIdListAddr, DWORD kickedWxIdListLen);
void Wx_QuitChatRoom(TCHAR* chatRoomId);

WxChatRoomInfo Wx_GetChatRoomInfo(TCHAR* chatRoomId, Wx_jsonBuff* jb, bool isSend = true);
void Wx_GetChatRoomInfo(TCHAR* chatRoomId, CHAR* chatRoomInfoBuff);
