﻿// dllmain.cpp : 定义 DLL 应用程序的入口点。
#include "stdafx.h"
#include "Wx_Init.h"
#include "Wx_GlobalData.h"
#include "Wx_GetMessage.h"
#include "Wx_OwnInfo.h"
#include "Wx_Friend.h"
#include "Wx_SendMessage.h"
#include "Wx_ChatRoom.h"
#include "Wx_Other.h"
#include "Wx_Database.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"
#include "include/rapidjson/document.h"
#include "Utils.h"
#include "Wx_WxObjectParse.h"
#include "Wx_Multi.h"

using namespace rapidjson;

void recvHandle(CHAR * recvBuff) {

	DWORD size = MultiByteToWideChar(CP_UTF8, 0, recvBuff, -1, NULL, 0);
	TCHAR* dataBuff = new TCHAR[size];
	if (size > 0) {

		MultiByteToWideChar(CP_UTF8, 0, recvBuff, -1, dataBuff, size);
		WxDataJsonParse jsonParse;
		jsonParse.buff = dataBuff;
		int type = -1;

		Wx_Parse_Wx_GetInfoByWxid infoParse;
		Wx_Parse_Wx_AgreeFriend agreeParse;
		Wx_Parse_Wx_DeleteFriend deleteFriendParse;
		Wx_Parse_Wx_GetChatRoomInfo getChatRoomParse;
		Wx_Parse_Wx_UpdateChatRoomPost updateChatRoomPostParse;
		Wx_Parse_Wx_UpdateChatRoomName updateCHatRoomNameParse;
		Wx_Parse_Wx_InviteFriendJoinChatRoom inviteFriendJoinChatRoomParse;
		Wx_Parse_Wx_CreateChatRoom createChatRoomParse;
		Wx_Parse_Wx_KickFriendInChatRoom kickFriendInChatRoomParse;
		Wx_Parse_Wx_QuitChatRoom quitChatRoomParse;
		Wx_Parse_Wx_SendTextMessage sendTextMessageParse;
		Wx_Parse_Wx_SendFriendCardMessage sendFriendCardMessageParse;
		Wx_Parse_Wx_SendImageMessage sendImageMessageParse;
		Wx_Parse_Wx_SendFileMessage sendFileMessageParse;
		Wx_Parse_Wx_SendUrlMessage sendUrlMessageParse; 
		Wx_Parse_Wx_SendChatRoomUrlMessage sendChatRoomUrlMessageParse; 
		Wx_Parse_Wx_SendSmallAppMessage sendSmallAppMessageParse;
		Wx_Parse_Wx_GetMoney getmoneyParse;
		Wx_Parse_Wx_OpenPreventRevoke openPreventRevokeParse;
		Wx_Parse_Wx_DatabaseExec databaseExecParse;
		Wx_Parse_Wx_AddFriend addFriendParse;

		if (jsonParse.parse() && jsonParse.parseType()) {
			if (wcscmp(L"Wx_GetOwnInfo", jsonParse.typeValue) == 0) {
				type = 0;
			}
			else if (wcscmp(L"Wx_GetFriendList", jsonParse.typeValue) == 0) {
				type = 1;
			}
			else if (wcscmp(L"Wx_GetInfoByWxid", jsonParse.typeValue) == 0) {
				if (infoParse.Parse(dataBuff)) {
					type = 2;
				}
			}
			else if (wcscmp(L"Wx_AgreeFriend", jsonParse.typeValue) == 0) {
				if (agreeParse.Parse(dataBuff)) {
					type = 3;
				}
			}
			else if (wcscmp(L"Wx_DeleteFriend", jsonParse.typeValue) == 0) {
				if (deleteFriendParse.Parse(dataBuff)) {
					type = 4;
				}
			}
			else if (wcscmp(L"Wx_GetChatRoomInfo", jsonParse.typeValue) == 0) {
				if (getChatRoomParse.Parse(dataBuff)) {
					type = 5;
				}
			}
			else if (wcscmp(L"Wx_UpdateChatRoomPost", jsonParse.typeValue) == 0) {
				if (updateChatRoomPostParse.Parse(dataBuff)) {
					type = 6;
				}
			}
			else if (wcscmp(L"Wx_UpdateChatRoomName", jsonParse.typeValue) == 0) {
				if (updateCHatRoomNameParse.Parse(dataBuff)) {
					type = 7;
				}
			}
			else if (wcscmp(L"Wx_InviteFriendJoinChatRoom", jsonParse.typeValue) == 0) {
				if (inviteFriendJoinChatRoomParse.Parse(dataBuff)) {
					type = 8;
				}
			}
			else if (wcscmp(L"Wx_CreateChatRoom", jsonParse.typeValue) == 0) {
				if (createChatRoomParse.Parse(dataBuff)) {
					type = 9;
				}
			}
			else if (wcscmp(L"Wx_KickFriendInChatRoom", jsonParse.typeValue) == 0) {
				if (kickFriendInChatRoomParse.Parse(dataBuff)) {
					type = 10;
				}
			}
			else if (wcscmp(L"Wx_QuitChatRoom", jsonParse.typeValue) == 0) {
				if (quitChatRoomParse.Parse(dataBuff)) {
					type = 11;
				}
			}
			else if (wcscmp(L"Wx_GetMessage", jsonParse.typeValue) == 0) {
				type = 12;
			}
			else if (wcscmp(L"Wx_CloseGetMessage", jsonParse.typeValue) == 0) {
				type = 13;
			}
			else if (wcscmp(L"Wx_SendTextMessage", jsonParse.typeValue) == 0) {
				if (sendTextMessageParse.Parse(dataBuff)) {
					type = 14;
				}
			}
			else if (wcscmp(L"Wx_SendFriendCardMessage", jsonParse.typeValue) == 0) {
				if (sendFriendCardMessageParse.Parse(dataBuff)) {
					type = 15;
				}
			}
			else if (wcscmp(L"Wx_SendImageMessage", jsonParse.typeValue) == 0) {
				if (sendImageMessageParse.Parse(dataBuff)) {
					type = 16;
				}
			}
			else if (wcscmp(L"Wx_SendFileMessage", jsonParse.typeValue) == 0) {
				if (sendFileMessageParse.Parse(dataBuff)) {
					type = 17;
				}
			}
			else if (wcscmp(L"Wx_SendUrlMessage", jsonParse.typeValue) == 0) {
				if (sendUrlMessageParse.Parse(dataBuff)) {
					type = 18;
				}
			}
			else if (wcscmp(L"Wx_SendChatRoomUrlMessage", jsonParse.typeValue) == 0) {
				if (sendChatRoomUrlMessageParse.Parse(dataBuff)) {
					type = 19;
				}
			}
			else if (wcscmp(L"Wx_SendSmallAppMessage", jsonParse.typeValue) == 0) {
				if (sendSmallAppMessageParse.Parse(dataBuff)) {
					type = 20;
				}
			}
			else if (wcscmp(L"Wx_GetMoney", jsonParse.typeValue) == 0) {
				if (getmoneyParse.Parse(dataBuff)) {
					type = 21;
				}
			}
			else if (wcscmp(L"Wx_OpenPreventRevoke", jsonParse.typeValue) == 0) {
				if (openPreventRevokeParse.Parse(dataBuff)) {
					type = 22;
				}
			}
			else if (wcscmp(L"Wx_ClosePreventRevoke", jsonParse.typeValue) == 0) {
				type = 23;
			}
			else if (wcscmp(L"Wx_DatabaseExec", jsonParse.typeValue) == 0) {
				if (databaseExecParse.Parse(dataBuff)) {
					type = 24;
				}
			}
			else if (wcscmp(L"Wx_AddFriend", jsonParse.typeValue) == 0) {
				if (addFriendParse.Parse(dataBuff)) {
					type = 25;
				}
			}
		}


		DWORD size = 0;
		Wx_jsonBuff jb;
		switch (type)
		{
		case 0:
			jb = Wx_GetWxOwnInfo(true);
			jb.Getstr(jsonParse.msgId);
			sctSocket.wxSend(jb);
			break;
		case 1:
			Wx_GetFriendList(&jb);
			jb.Getstr(jsonParse.msgId);
			sctSocket.wxSend(jb);
			break;
		case 2:
			Wx_GetInfoByWxid(infoParse.wxid, &jb);
			jb.Getstr(jsonParse.msgId);
			sctSocket.wxSend(jb);
			break;
		case 3:
			Wx_AgreeFriend(agreeParse.v1, agreeParse.v2);
			break;
		case 4:
			Wx_DeleteFriend(deleteFriendParse.wxid);
			break;
		case 5:
			Wx_GetChatRoomInfo(getChatRoomParse.chatRoomId, &jb);
			jb.Getstr(jsonParse.msgId);
			sctSocket.wxSend(jb);
			break; 
		case 6:
			Wx_UpdateChatRoomPost(updateChatRoomPostParse.chatRoomId, updateChatRoomPostParse.text);
			break;
		case 7:
			Wx_UpdateChatRoomName(updateCHatRoomNameParse.chatRoomId, updateCHatRoomNameParse.newName);
			break;
		case 8:
			Wx_InviteFriendJoinChatRoom(inviteFriendJoinChatRoomParse.chatRoomId, inviteFriendJoinChatRoomParse.inviteeWxIdListAddr, inviteFriendJoinChatRoomParse.inviteeWxIdListLen);
			break;
		case 9:
			Wx_CreateChatRoom(createChatRoomParse.inviteeWxIdListAddr, createChatRoomParse.inviteeWxIdListLen);
			break;
		case 10:
			Wx_KickFriendInChatRoom(kickFriendInChatRoomParse.chatRoomId, kickFriendInChatRoomParse.inviteeWxIdListAddr, kickFriendInChatRoomParse.inviteeWxIdListLen);
			break;
		case 11:
			Wx_QuitChatRoom(quitChatRoomParse.chatRoomId);
			break;
		case 12:
			if (Wx_GetMessage()) {
				SendSuccess((TCHAR*)L"Wx_GetMessage", (TCHAR*)L"open GetMessage success", jsonParse.msgId);
			}
			else {
				SendError((TCHAR*)L"Wx_GetMessage", (TCHAR*)L"open GetMessage error", jsonParse.msgId);
			}
			break;
		case 13:
			if (Wx_CloseGetMessage()) {
				SendSuccess((TCHAR*)L"Wx_CloseGetMessage", (TCHAR*)L"open CloseGetMessage success", jsonParse.msgId);
			}
			else {
				SendError((TCHAR*)L"Wx_CloseGetMessage", (TCHAR*)L"open CloseGetMessage error", jsonParse.msgId);
			}
			break;
		case 14:
			Wx_SendTextMessage(sendTextMessageParse.wxid, sendTextMessageParse.texta, sendTextMessageParse.atListAddr, sendTextMessageParse.atListLen);
			break;
		case 15:
			Wx_SendFriendCardMessage(sendFriendCardMessageParse.wxid, sendFriendCardMessageParse.cardWxId);
			break;
		case 16:
			Wx_SendImageMessage(sendImageMessageParse.wxid, sendImageMessageParse.filepath);
			break; 
		case 17:
			Wx_SendFileMessage(sendFileMessageParse.wxid, sendFileMessageParse.filepath);
			break; 
		case 18:
			Wx_SendUrlMessage(sendUrlMessageParse.wxid, sendUrlMessageParse.articleInfo);
			break;
		case 19:
			Wx_SendChatRoomUrlMessage(sendChatRoomUrlMessageParse.chatRoomId, sendChatRoomUrlMessageParse.wxid);
			break;
		case 20:
			Wx_SendSmallAppMessage(sendSmallAppMessageParse.wxid, sendSmallAppMessageParse.smallAppInfo);
			break;
		case 21:
			Wx_GetMoney(getmoneyParse.wxid, getmoneyParse.transid);
			break; 
		case 22:
			Wx_OpenPreventRevoke(openPreventRevokeParse.text);
			break;
		case 23:
			if (Wx_ClosePreventRevoke()) {
				SendSuccess((TCHAR*)L"Wx_ClosePreventRevoke", (TCHAR*)L"open ClosePreventRevoke success", jsonParse.msgId);
			}
			else {
				SendError((TCHAR*)L"Wx_ClosePreventRevoke", (TCHAR*)L"open ClosePreventRevoke error", jsonParse.msgId);
			}
			break;
		case 24:
			size = WideCharToMultiByte(CP_UTF8, 0, databaseExecParse.sql, -1, 0, 0, 0, 0);
			if (size > 0) {
				CHAR* buff = new CHAR[size];
				WideCharToMultiByte(CP_UTF8, 0, databaseExecParse.sql, -1, buff, size, 0, 0);
				Wx_DatabaseExec(databaseExecParse.dbName, buff, &jb);
				jb.Getstr(jsonParse.msgId);
				sctSocket.wxSend(jb);
				delete[] buff;
			}
			else {
				SendError((TCHAR*)L"Wx_DatabaseExec", (TCHAR*)L"sqlTransError", jsonParse.msgId);
			}
			break; 
		case 25:
			Wx_AddFriend(addFriendParse.wxid, addFriendParse.text, addFriendParse.type);
			break;
		default:
			SendError((TCHAR*)L"Wx_CommandError", (TCHAR*)L"jsonParseError", jsonParse.msgId);
			break;
		}

		
		
	}
	else {
		SendError((TCHAR*)L"Wx_JsonParseError", (TCHAR*)L"jsonParseError");
	}

	delete[] dataBuff;
	delete[] recvBuff;
	
}

void ChatRecv() {

	Wx_GetDatabaseHandle();

	//连接server, 没连接上不准下一步操作
	while (true) {
		if (sctSocket.wxConnect() == SOCKET_ERROR) {
		}
		else {
			break;
		}
	}


	while (true) {


		/*
			是否已经登录, 没登录，不准下一步操作
			这里判断是否已登录, 
			如果已经登录, 初始化并构建json，发送给server
		*/
		if (Wx_IsLogin()) {
			//init 与 getWxOwnInfo 必须先调用一下
			Wx_Init();
			Wx_GetWxOwnInfo(false);
			/*
			Wx_Multi wxMulti;
			if (wxMulti.Open()) {
				SendSuccess((TCHAR*)L"Wx_Multi", (TCHAR*)L"Wx_Multi success");
			}
			else {
				SendError((TCHAR*)L"Wx_Multi", (TCHAR*)L"Wx_Multi fail");
			}
			*/
			//发送客户端socket给服务端, 以便服务端可以主动发送消息给客户端
			StringBuffer s;
			Writer<StringBuffer, UTF16<>> writer(s);
			writer.StartObject();
			writer.Key(L"type");
			writer.String(L"Wx_SendClient");
			writer.Key(L"status");
			writer.String(L"success");
			writer.Key(L"wxid");
			writer.String(wxOwnInfo.wxId);
			writer.EndObject();
			sctSocket.wxSend((CHAR*)s.GetString(), s.GetSize());
			break;

		}

	}


	while (true) {
		sctSocket.wxRecv(recvHandle);
	}

}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{

    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
		hWeChatWin = LoadLibrary(L"WeChatWin.dll");
		CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ChatRecv, NULL, 0, 0);
		break;
    case DLL_THREAD_ATTACH:
		break;
    case DLL_THREAD_DETACH:
		break;
    case DLL_PROCESS_DETACH:

        break;
    }
    return TRUE;
}

