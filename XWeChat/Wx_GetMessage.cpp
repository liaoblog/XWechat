#include "stdafx.h"
#include "Wx_GlobalData.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"
#include "Utils.h"


using namespace rapidjson;

//消息结构体
struct SctMsg {
	//消息发送者与长度
	CHAR* fromUser;
	DWORD fromUserLen;

	//消息接收者与长度
	CHAR* toUser;
	DWORD toUserLen;

	//消息内容、长度、类型
	CHAR* msgContent;
	DWORD msgContentLen;
	DWORD msgContentType;

	//消息中at用户列表
	CHAR* atUserList;
	DWORD atUserListLen;

	//具体消息发送者、长度（如果消息为群消息，有值；否则为0）
	CHAR* detailFromUser;
	DWORD detailFromUserLen;
};

void MsgHandle(SctMsg* sctMsg);
void MsgPack(SctMsg* sctMsg);

DWORD addrOldGetMessageCall;
DWORD addrGetMessageNextOffest;

void  _getMessage() {	

	SctMsg sctMsg = { 0 };
	DWORD Flag;
	DWORD isAtList;
	__asm {
		//数据操作-发送者
		mov edi, [ebp]
		lea edi, [edi - 0x770]
		mov edi, [edi]
		mov sctMsg.fromUserLen, edi

		mov edi, [ebp]
		lea edi, dword ptr ds : [edi - 0x774]
		mov edi, [edi]
		mov sctMsg.fromUser, edi

		/*
		//数据操作-接收者
		mov edi, [ebp]
		lea edi, dword ptr ds : [edi - 0x570]
		mov edi, [edi]
		mov sctMsg.toUserLen, edi

		mov edi, [ebp]
		lea edi, dword ptr ds : [edi - 0x574]
		mov edi, [edi]
		mov sctMsg.toUser, edi
		*/

		//数据操作-消息内容
		mov edi, [ebp]
		lea edi, dword ptr ds : [edi - 0x74c]
		mov edi, [edi]
		mov sctMsg.msgContent, edi

		mov edi, [ebp]
		lea edi, dword ptr ds : [edi - 0x748]
		mov edi, [edi]
		mov sctMsg.msgContentLen, edi

		mov edi, [ebp]
		lea edi, dword ptr ds : [edi - 0x784]
		mov edi, [edi]
		mov sctMsg.msgContentType, edi

		//数据操作-具体发送者
		mov edi, [ebp]
		lea edi, dword ptr ds : [edi - 0x6a0]
		mov edi, [edi]
		mov Flag, edi

		//数据操作-at 用户列表
		mov edi, [ebp]
		lea edi, dword ptr ds : [edi - 0x5b0]
		mov edi, [edi]
		mov isAtList, edi

	}

	if (Flag != 0) {
		__asm {

			//数据操作-具体发送者
			mov edi, [ebp]
			lea edi, dword ptr ds : [edi - 0x69c]
			mov edi, [edi]
			mov sctMsg.detailFromUserLen, edi

			mov edi, [ebp]
			lea edi, dword ptr ds : [edi - 0x6a0]
			mov edi, [edi]
			mov sctMsg.detailFromUser, edi
		}

		
	}

	if (isAtList != 0) {
		__asm {

			//数据操作-at 用户列表
			mov edi, [ebp]
			lea edi, dword ptr ds : [edi - 0x5b0]
			mov edi, [edi]
			mov sctMsg.atUserListLen, edi

			mov edi, [ebp]
			lea edi, dword ptr ds : [edi - 0x5b4]
			mov edi, [edi]
			mov sctMsg.atUserList, edi
		}


	}

	MsgPack(&sctMsg);

}


void __declspec(naked) _TempStroage() 
{

	__asm 
	{
		pushad
		pushfd
		call _getMessage
		popfd
		popad
		call addrOldGetMessageCall
		jmp addrGetMessageNextOffest
	}

}

void MsgPack(SctMsg* sctMsg) {

	StringBuffer s;
	Writer<StringBuffer, UTF16<>> writer(s);

	writer.StartObject();

	writer.Key(L"type");
	writer.String(L"Wx_GetMessage");

	writer.Key(L"status");
	writer.String(L"success");

	writer.Key(L"wxid");
	writer.String(wxOwnInfo.wxId);

	writer.Key(L"content");

	writer.StartObject();

	writer.Key(L"fromUser");
	writer.String((TCHAR*)sctMsg->fromUser);
	writer.Key(L"fromUserLen");
	writer.Int(sctMsg->fromUserLen);

	/*
	writer.Key(L"toUser");
	writer.String((TCHAR*)sctMsg->toUser);
	writer.Key(L"toUserLen");
	writer.Int(sctMsg->toUserLen);
	*/

	writer.Key(L"msgContent");
	writer.String((TCHAR*)sctMsg->msgContent);

	writer.Key(L"msgContentLen");
	writer.Int(sctMsg->msgContentLen);

	writer.Key(L"msgContentType");
	writer.Int(sctMsg->msgContentType);

	if (sctMsg->detailFromUserLen != 0) {

		writer.Key(L"detailFromUser");
		writer.String((TCHAR*)sctMsg->detailFromUser);

		writer.Key(L"detailFromUserLen");
		writer.Int(sctMsg->detailFromUserLen);

	}

	if (sctMsg->atUserListLen != 0) {

		writer.Key(L"atUserList");
		writer.String((TCHAR*)sctMsg->atUserList);

		writer.Key(L"atUserListLen");
		writer.Int(sctMsg->atUserListLen);

	}

	writer.EndObject();

	writer.EndObject();

	sctSocket.wxSend((CHAR*)s.GetString(), s.GetSize());

}

void MsgHandle(SctMsg* sctMsg) {

	switch (sctMsg->msgContentType)
	{
	//text
	case  0x1:     // 文本/表情/腾讯新闻

		break;

	//image
	case 0x3:     //  图片
		break;

	//voice
	case 0x22:     // 语音
		break;

	//card
	case 0x2A:     // 名片
		break;

	//video
	case 0x2B:     // 视频 
		break;

	//emoji
	case 0x2F:     // 表情包
		break;

	//location
	case 0x30:     // 位置
		break;

	//xml
	case 0x31:     // 转账/链接/文件/位置实时共享/公号消息/聊天记录等
		break;

	//smalltext
	case 0x2710:   // 灰字消息（红包、位置结束共享等）
		break;

	//其他消息不处理
	default:       // 其他消息类型忽略,如: sysmsg
		break;
	}
}


bool Wx_GetMessage() 
{

	PrintFunAddr((DWORD)Wx_GetMessage);

	if (!isOpenGetMessage) {


		addrOldGetMessageCall = (DWORD)hWeChatWin + ADDR_GETMESSAGE_CALL_OFFEST;
		DWORD addrGetMessageOffest = (DWORD)hWeChatWin + ADDR_GETMESSAGE_OFFEST;
		addrGetMessageNextOffest = addrGetMessageOffest + 0x5;

		DWORD addrHookMachineCode = (DWORD)_TempStroage - addrGetMessageNextOffest;

		BYTE machineCode[5];
		machineCode[0] = 0xe9;
		CHAR* addrTempHookMachineCode = (CHAR*)&addrHookMachineCode;
		machineCode[1] = *(addrTempHookMachineCode + 0);
		machineCode[2] = *(addrTempHookMachineCode + 1);
		machineCode[3] = *(addrTempHookMachineCode + 2);
		machineCode[4] = *(addrTempHookMachineCode + 3);

		BOOL flag = WriteProcessMemory((HANDLE)-1, (LPVOID) * &addrGetMessageOffest, machineCode, 5, NULL);
		if (flag > 0) {
			isOpenGetMessage = 1;
			return isOpenGetMessage;
		}
		else {
			return isOpenGetMessage;
		}

	}

	return isOpenGetMessage;

}


bool Wx_CloseGetMessage() {

	PrintFunAddr((DWORD)Wx_CloseGetMessage);

	if (isOpenGetMessage) {

		addrOldGetMessageCall = (DWORD)hWeChatWin + ADDR_GETMESSAGE_CALL_OFFEST;
		DWORD addrGetMessageOffest = (DWORD)hWeChatWin + ADDR_GETMESSAGE_OFFEST;
		addrGetMessageNextOffest = addrGetMessageOffest + 0x5;

		DWORD addrHookMachineCode = addrOldGetMessageCall - addrGetMessageNextOffest;

		BYTE machineCode[5];
		machineCode[0] = 0xe8;
		CHAR* addrTempHookMachineCode = (CHAR*)&addrHookMachineCode;
		machineCode[1] = *(addrTempHookMachineCode + 0);
		machineCode[2] = *(addrTempHookMachineCode + 1);
		machineCode[3] = *(addrTempHookMachineCode + 2);
		machineCode[4] = *(addrTempHookMachineCode + 3);

		BOOL flag = WriteProcessMemory((HANDLE)-1, (LPVOID) * &addrGetMessageOffest, machineCode, 5, NULL);
		if (flag > 0) {
			isOpenGetMessage = 0;
			return 1;
		}
		else {
			return 0;
		}

	}
	return 1;

}
