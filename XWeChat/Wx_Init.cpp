#include "stdafx.h"
#include <Windows.h>
#include "Wx_GlobalData.h"
#include <winsock.h>
#include < WS2tcpip.h >
#include "Wx_OwnInfo.h"
#include "Wx_HostConfig.h"
#include "Wx_Utils.h"
#include "Utils.h"
#include "include/rapidjson/stringbuffer.h"
#include "include/rapidjson/document.h"
typedef GenericDocument<UTF8<> > WDocumentBuild;
typedef GenericValue<UTF8<> > WValueBuild;
using namespace rapidjson;
#pragma comment(lib, "ws2_32.lib")

//定义全局数据
HANDLE hWeChatWin;
DWORD pFriendListHeadPointer;
WxSocket sctSocket;
WxOwnInfo wxOwnInfo;
bool isPrint;
CHAR* preventRevokeTip;
DWORD isPreventRevoke;
DWORD isOpenGetMessage;
DWORD isOpenForwardRevokeMsg;
std::map<std::wstring, DWORD> databaseMap;


WxOwnInfo::WxOwnInfo() {
	
	init();
}

void WxOwnInfo::init() {
	country = NULL;

	city = NULL;

	provice = NULL;

	wxId = NULL;

	wxPhoneNumber = NULL;

	wxNumber = NULL;

	loginDevice = NULL;

	nickName = NULL;

	headImg = NULL;

	signature = NULL;

	fileSavePath = NULL;
}

WxOwnInfo::~WxOwnInfo() {
	free((DWORD*)country);
	free((DWORD*)city);

	free((DWORD*)provice);

	free((DWORD*)wxId);

	free((DWORD*)wxPhoneNumber);

	free((DWORD*)wxNumber);

	free((DWORD*)loginDevice);

	free((DWORD*)nickName);

	free((DWORD*)headImg);

	free((DWORD*)signature);

	free((DWORD*)fileSavePath);

	init();

}

WxSocket::WxSocket() {

	WSADATA wsadata;
	int err = WSAStartup(MAKEWORD(2, 2), &wsadata);

	if (err != 0) {
		MessageBox(NULL, L"客户端初始化失败", 0, 0);
	}

	server_addr.sin_family = AF_INET;
	//server_addr.sin_addr.S_un.S_addr = Inet_pton(HOST);
	InetPton(AF_INET, TEXT(HOST), &server_addr.sin_addr.s_addr);
	server_addr.sin_port = htons(PORT);

	this->s_server = socket(AF_INET, SOCK_STREAM, 0);

	

}

int WxSocket::wxConnect() {

	return connect(s_server, (SOCKADDR*)&server_addr, sizeof(SOCKADDR));

}

void WxSocket::wxSend(CHAR* buff, DWORD len) {

	send(s_server, buff, len + 1, 0);

}

void WxSocket::wxSend(Wx_jsonBuff s) {
	send(s_server, s.str.c_str(), s.len + 1, 0);
}

void WxSocket::wxRecv(void(*callback)(CHAR* recvBuff)) {

	while (1) {
		CHAR flagBuff[0x1] = { 0 };
		int i = 0;
		i = recv(s_server, flagBuff, 0x1, 0);
		if (i > 0) {
			if (*flagBuff == 0x7e) {
				break;
			}
		}
	}

	CHAR lenBuff[0xA] = { 0 };
	recv(s_server, lenBuff, 0xA, 0);
	DWORD size = atoi(lenBuff);
	if (size > 0) {
		CHAR* dataBuff = new CHAR[size + 1];
		recv(s_server, dataBuff, size, 0);
		dataBuff[size] = '\0';
		callback(dataBuff);
	}

}

WxSocket::~WxSocket() {
	closesocket(this->s_server);
	WSACleanup();
}


WxStr::WxStr() {
	str = 0;
	len = 0;
	maxLen = 0;
	zero = 0;
	zero2 = 0;
}

void WxStr::countLen() {
	len = wcslen(str);
	maxLen = len * 2;
}

WxFileStr::WxFileStr() {

	msgType = 0;
	str = 0;
	len = 0;
	maxLen = 0;
	zero = 0;
	zero2 = 0;
	zero3 = 0;
	zero4 = 0;
	zero5 = 0;

}

void WxFileStr::countLen() {
	len = wcslen(str);
	maxLen = len * 2;
}

WxAddrSct::WxAddrSct() {

	startAddr = 0;
	endAddr = 0;
	endAddr2 = 0;

}

void Wx_jsonBuff::Getstr(TCHAR* msgId) {
	
	if (msgId) {
		WDocumentBuild doc;
		WValueBuild val;

		doc.Parse(str.c_str());
		if (!doc.HasParseError()) {

			DWORD size = WideCharToMultiByte(CP_UTF8, 0, msgId, -1, 0, 0, 0, 0);
			CHAR* buff = new CHAR[size];
			WideCharToMultiByte(CP_UTF8, 0, msgId, -1, buff, size, 0, 0);
			std::string tempstr= std::string(buff);
			delete[] buff;

			doc.AddMember(StringRef("msgId"), StringRef(tempstr.c_str()), doc.GetAllocator());

			rapidjson::StringBuffer buffer;
			rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
			doc.Accept(writer);

			str = std::string(buffer.GetString());
			len = buffer.GetSize();

		}


	}

}

void Wx_Init() {
	wxOwnInfo.init();
	isPrint = true;
	preventRevokeTip = new CHAR[51];
	isPreventRevoke = 0;
	isOpenForwardRevokeMsg = 0;
	isOpenGetMessage = 0;
	Wx_GetFriendListHeadPointer(&pFriendListHeadPointer);
}