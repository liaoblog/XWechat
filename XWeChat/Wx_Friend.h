#pragma once
WxUserInfo Wx_GetInfoByWxid(TCHAR* wxid, Wx_jsonBuff* jb, bool isSend = true);
void Wx_GetInfoByWxid(TCHAR* wxid, CHAR* friendInfoBuff);
void Wx_AgreeFriend(TCHAR* v1, TCHAR* v2);
void Wx_AddFriend(TCHAR* addedWxid, TCHAR* text, DWORD addWay = 0x6);
void Wx_DeleteFriend(TCHAR* deletedWxId);
void Wx_GetFriendList(Wx_jsonBuff* jb);